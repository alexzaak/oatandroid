# OwnAntiTheft
###### von Alexander Zaak und Max Edenharter


## Überblick
OwnAntiTheft (OAT) ist ein Anti-Diebstahlsystem für sämtliche Android-Geräte ab Version 4.1. Es besteht aus einer App und einer Webanwendung.

Ziel ist es das Gerät im Falle eines Diebstahls über eine Webanwendung zu sperren, orten oder zumindest persönliche Daten zu sichern, um sie anschließend vom Gerät zu vernichten. Alternativ ist es möglich alle Funktionen via SMS zu steuern. Dies  ist vor allem dann nützlich, wenn die Netzabdeckung für mobiles Internet unzureichend ist.

Anders als viele andere Systeme, ist OwnAntiTheft ein dezentrales System. Die Webanwendung kann auf vielen kostenlosen Webspace-Anbietern betrieben werden. Eine einzelne Installation kann mehrere Benutzer verwalten, sodass es problemlos möglich ist, die eigene Familie mit diesem Dienst zu versorgen.

## Funktionsumfang
Funktion                                        |Bedingung| SMS-Befehl          | Verfügbarkeit
------------------------------------------------|---------| --------------------| --------------
Geräte-Status ermitteln                         |    /    | oat status          |
Gerät sperren                                   |    DA   | oat lock {MESSAGE}  | API level  8
Alarm                                           |    /    | oat alarm {DAUER}   |
Passwort zurücksetzen                           |    DA   | oat password {PASSWORT} | API level  8
Gerätespeicher ver-/entschlüsseln               |    /    | oat encrypt  {0/1}  | API level  8
Gerät zurücksetzen (Speicher löschen / Einstellungen) | DA| oat wipe [all]      | API level  8
Kamera de-/aktivieren                           |    DA   | oat camera {0/1}    | API level 14
Synchronistation de-/aktivieren                 |    /    | oat sync {0/1}      |
Wifi de-/aktivieren                             |    /    | oat wifi {0/1}      | API level  8
Bluetooth de-/aktivieren                        |    /    | oat bt {0/1}        | API level  8
GPS de-/aktivieren                              |    SA   | oat gps {0/1}       | API level 21
Displayhelligkeit regulieren                    |    /    | oat bright {0/1}    | API level  8
Ortung mit Hilfe von GPS                        |    /    | oat locate {DAUER}  | API level  8
Kontakte sichern                                |    /    | oat contacts        | API level  8
Rückruf                                         |    /    | oat callback        | API level  8

###### Legende:
 * DA - Geräteadministrator
 * SA - System-App
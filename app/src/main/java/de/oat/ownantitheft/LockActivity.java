package de.oat.ownantitheft;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.device.Connectivity;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.remote.SMSSender;
import de.oat.ownantitheft.services.LockService;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.EmailSender;
import de.oat.ownantitheft.utils.InputValidator;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * @author Alexander Zaak
 * 18.08.15.
 */
public class LockActivity extends Activity {
    private static final Logger LOGGER = Logger.getLogger(LockActivity.class);

    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lockscreen);
        prefManager = new PrefManager();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        TextView lockOwnerInfo = (TextView) this.findViewById(R.id.lock_owner_info);

        ImageView callBtn = (ImageView) this.findViewById(R.id.call_owner_btn);
        ImageView unlockBtn = (ImageView) this.findViewById(R.id.unlock_btn);
        ImageView messageBtn = (ImageView) this.findViewById(R.id.message_btn);
        EditText messageToOwner = (EditText) this.findViewById(R.id.message_to_owner_edit);


        if (!Connectivity.hasTelephonyFeature()) {
            callBtn.setVisibility(View.GONE);
        }


        lockOwnerInfo.setText(prefManager.getOwnerInfo());
        callBtn.setOnClickListener(createCallClickListener());
        unlockBtn.setOnClickListener(createUnlockListener());
        messageBtn.setOnClickListener(createMessageClickListener(messageToOwner));

        messageToOwner.setOnEditorActionListener(createEditorActionListener(messageToOwner));
    }


    /**
     * @param messageToOwner
     * @return
     */
    private EditText.OnEditorActionListener createEditorActionListener(final EditText messageToOwner) {
        return new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND && InputValidator.notEmpty(LockActivity.this, messageToOwner)) {
                    handled = true;
                    messageToOwner.setVisibility(View.GONE);
                    sendMessage(messageToOwner.getText().toString());

                }
                return handled;
            }
        };
    }

    /**
     *
     * @param message
     */
    private void sendMessage(String message) {
        LOGGER.debug(message);
        if (Connectivity.hasTelephonyFeature()) {
            SMSSender.sendSms(prefManager.getOwnerPhoneNumber(), message);
        }
        try {
            EmailSender.sendMessage(getString(R.string.your_phone_was_found), message);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
    }

    /**
     *
     * @param messageToOwner
     * @return
     */
    private View.OnClickListener createMessageClickListener(final EditText messageToOwner) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageToOwner.getVisibility() == View.VISIBLE) {
                    messageToOwner.setVisibility(View.GONE);
                } else {
                    messageToOwner.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /**
     *
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus) {
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);

        }

        super.onWindowFocusChanged(hasFocus);
    }

    /**
     *
     */
    @Override
    protected void onPause() {
        super.onPause();
        showCountDown(Const.HIDE_LOCK_ACTIVITY);
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        showCountDown(Const.CANCEL_LOCK_COUNTDOWN);
    }


    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return disableKeyEvents(keyCode) || super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        return disableKeyEvents(keyCode) || super.onKeyDown(keyCode, event);
    }


    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event) {
        return disableKeyEvents(keyCode) || super.onKeyDown(keyCode, event);

    }

    /**
     *
     * @param keyCode
     * @return
     */
    private boolean disableKeyEvents(int keyCode) {
        return keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_POWER || keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_CAMERA;
    }

    /**
     *
     * @return
     */
    private View.OnClickListener createCallClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeviceController.callMe(prefManager.getOwnerPhoneNumber());
            }
        };
    }

    /**
     *
     * @return
     */
    private View.OnClickListener createUnlockListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minimizeActivity();
                showCountDown(Const.HIDE_LOCK_ACTIVITY);
            }
        };
    }

    /**
     *
     */
    private void minimizeActivity() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    /**
     *
     * @param command
     */
    private void showCountDown(int command) {
        final Intent serviceIntent = new Intent(this, LockService.class);
        serviceIntent.putExtra(Const.LOCK_ACTIVITY_COMMAND, command);
        if (prefManager.isLocked()) {
            startService(serviceIntent);
        } else {
            stopService(serviceIntent);
        }
    }
}
package de.oat.ownantitheft.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.log4j.Logger;

import de.oat.ownantitheft.model.OATCommands;
import de.oat.ownantitheft.receiver.GcmBroadcastReceiver;
import de.oat.ownantitheft.remote.OATSubCommander;

/**
 * Created by Alexander Zaak on 17.08.15.
 */
public class GcmMessageService extends IntentService {

    private static final Logger LOGGER = Logger.getLogger(GcmMessageService.class);

    public GcmMessageService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        OATSubCommander commander = new OATSubCommander();

        OATCommands OATCommands;

        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                LOGGER.debug("Conflicted messages on server: "
                        + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                LOGGER.debug("Deleted messages on server: "
                        + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                OATCommands = new OATCommands(extras);
                LOGGER.debug("OATCommands Received from Google GCM Server: [ CNO:" + OATCommands.getCno() + " extras: " + intent.getExtras().toString() + " ]");
                commander.controlDevice(OATCommands, null);
            }

        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }


}
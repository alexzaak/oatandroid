package de.oat.ownantitheft.services;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import org.apache.log4j.Logger;

import de.oat.ownantitheft.network.EncryptionTask;
import de.oat.ownantitheft.receiver.EncryptionReceiver;
import de.oat.ownantitheft.utils.Const;

public class EncryptService extends Service implements EncryptionReceiver.OnEncryptionTaskListener {

    private static final Logger LOGGER = Logger.getLogger(EncryptService.class);
    private EncryptionReceiver encryptionReceiver;

    public EncryptService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        encryptionReceiver = new EncryptionReceiver();
        encryptionReceiver.setListener(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Const.ENCRYPTION_FINISHED);
        registerReceiver(encryptionReceiver, filter);


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            boolean encrypt = intent.getBooleanExtra(Const.ENCRYPTION, false);
            EncryptionTask encryptionTask = new EncryptionTask(this, encrypt);
            encryptionTask.execute();
        }


        return START_REDELIVER_INTENT;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(encryptionReceiver);
    }


    @Override
    public void onEncrypted() {
        this.stopSelf();
    }
}


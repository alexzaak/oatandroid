package de.oat.ownantitheft.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.model.LocationData;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.remote.SMSSender;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Created by Alexander Zaak on 24.09.15.
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final Logger LOGGER = Logger.getLogger(LocationService.class);
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private PrefManager prefManager;
    private String phoneNumber;

    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        prefManager = new PrefManager();

        // Building the GoogleApi client
        buildGoogleApiClient();
        createLocationRequest();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    /**
     * Called by the system every time a client explicitly starts the service by calling
     * {@link Context#startService}, providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     * <p/>
     * <p>For backwards compatibility, the default implementation calls
     * {@link #onStart} and returns either {@link #START_STICKY}
     * or {@link #START_STICKY_COMPATIBILITY}.
     * <p/>
     * <p>If you need your application to run on platform versions prior to API
     * level 5, you can use the following model to handle the older {@link #onStart}
     * callback in that case.  The <code>handleCommand</code> method is implemented by
     * you as appropriate:
     * <p/>
     * {@sample development/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.java
     * start_compatibility}
     * <p/>
     * <p class="caution">Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use {@link AsyncTask}.</p>
     *
     * @param intent  The Intent supplied to {@link Context#startService},
     *                as given.  This may be null if the service is being restarted after
     *                its process has gone away, and it had previously returned anything
     *                except {@link #START_STICKY_COMPATIBILITY}.
     * @param flags   Additional data about this start request.  Currently either
     *                0, {@link #START_FLAG_REDELIVERY}, or {@link #START_FLAG_RETRY}.
     * @param startId A unique integer representing this specific request to
     *                start.  Use with {@link #stopSelfResult(int)}.
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the {@link #START_CONTINUATION_MASK} bits.
     * @see #stopSelfResult(int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent == null) {
            return START_REDELIVER_INTENT;
        }

        int duration = intent.getIntExtra(Const.GPS_DURATION, 30 * 60 * 100);
        boolean enabled = intent.getBooleanExtra(Const.GPS_ENABLE, false);
        phoneNumber = intent.getStringExtra(Const.GPS_PHONENUMBER);


        if (enabled) {
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
            // wait duration... then stop the locationservice.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stopService();
                }
            }, duration);//millisec.
        } else {
            stopService();
        }


        return START_REDELIVER_INTENT;
    }

    /**
     * Called by the system to notify a Service that it is no longer used and is being removed.  The
     * service should clean up any resources it holds (threads, registered
     * receivers, etc) at this point.  Upon return, there will be no more calls
     * in to this Service object and it is effectively dead.  Do not call this method directly.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p/>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Creating location request object
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(1);
        mLocationRequest.setFastestInterval(1);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Starting the location updates
     */
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Stopping location updates
     */
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    /**
     * Creating google api client object
     */
    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void getLocation(Location mLastLocation) throws UnsupportedEncodingException {
        if (mLastLocation != null) {
            LocationData locationData = new LocationData();
            locationData.setLat(mLastLocation.getLatitude());
            locationData.setLng(mLastLocation.getLongitude());
            locationData.setAccuracy(Math.round(mLastLocation.getAccuracy()));

            DeviceController.sendLocation(locationData);

            if (phoneNumber != null) {
                SMSSender.sendSms(phoneNumber, locationData.toString());
            }
        }
    }

    private void stopService() {
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
        stopSelf();
    }


    @Override
    public void onConnected(Bundle arg0) {

        if (!PermissionsCheck.hasPermissionLocation()) {
            LOGGER.error("Permission denied for request Location");
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        // Once connected with google api, get the location
        try {
            getLocation(mLastLocation);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
        startLocationUpdates();

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            getLocation(location);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LOGGER.error(connectionResult.getErrorCode());
    }
}

package de.oat.ownantitheft.services;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.apache.log4j.Logger;

import de.oat.ownantitheft.LockActivity;
import de.oat.ownantitheft.R;
import de.oat.ownantitheft.receiver.LockReceiver;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Created by Alexander Zaak on 18.08.15.
 */
public class LockService extends Service {

    private static final Logger LOGGER = Logger.getLogger(LockService.class);

    private BroadcastReceiver lockReceiver;
    private PrefManager prefManager;

    private TextView countDownHead;

    private WindowManager windowManager;
    private CountDownTimer countDownTimer;

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p/>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        countDownHead = new TextView(this);
        countDownHead.setBackgroundResource(R.mipmap.ic_head);
        countDownHead.setVisibility(View.GONE);
        countDownHead.setGravity(Gravity.CENTER);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
                PixelFormat.TRANSLUCENT);


        params.gravity = Gravity.TOP | Gravity.END;
        params.x = 0;
        params.y = 100;

        if (PermissionsCheck.hasPermissionDrawOverlays()) {
            windowManager.addView(countDownHead, params);
        }


        countDownTimer = new CountDownTimer(10000, 500) {

            public void onTick(long millisUntilFinished) {
                countDownHead.setText(String.valueOf(millisUntilFinished / 1000));
                countDownHead.setVisibility(View.VISIBLE);
                preventSystemDialogs();
            }

            public void onFinish() {
                runAsForeground();
                countDownHead.setVisibility(View.GONE);
            }
        };

        prefManager = new PrefManager();
        lockReceiver = new LockReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(lockReceiver, filter);

    }


    /**
     * Called by the system to notify a Service that it is no longer used and is being removed.  The
     * service should clean up any resources it holds (threads, registered
     * receivers, etc) at this point.  Upon return, there will be no more calls
     * in to this Service object and it is effectively dead.  Do not call this method directly.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownHead != null && countDownTimer != null) {
            countDownTimer.cancel();
            if (PermissionsCheck.hasPermissionDrawOverlays()) {
                try {
                    windowManager.removeView(countDownHead);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e);
                }
            }
        }
        unregisterReceiver(lockReceiver);
    }

    /**
     * Called by the system every time a client explicitly starts the service by calling
     * {@link Context#startService}, providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     * <p/>
     * <p>For backwards compatibility, the default implementation calls
     * {@link #onStart} and returns either {@link #START_STICKY}
     * or {@link #START_STICKY_COMPATIBILITY}.
     * <p/>
     * <p>If you need your application to run on platform versions prior to API
     * level 5, you can use the following model to handle the older {@link #onStart}
     * callback in that case.  The <code>handleCommand</code> method is implemented by
     * you as appropriate:
     * <p/>
     * {@sample development/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.java
     * start_compatibility}
     * <p/>
     * <p class="caution">Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use {@link AsyncTask}.</p>
     *
     * @param intent  The Intent supplied to {@link Context#startService},
     *                as given.  This may be null if the service is being restarted after
     *                its process has gone away, and it had previously returned anything
     *                except {@link #START_STICKY_COMPATIBILITY}.
     * @param flags   Additional data about this start request.  Currently either
     *                0, {@link #START_FLAG_REDELIVERY}, or {@link #START_FLAG_RETRY}.
     * @param startId A unique integer representing this specific request to
     *                start.  Use with {@link #stopSelfResult(int)}.
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the {@link #START_CONTINUATION_MASK} bits.
     * @see #stopSelfResult(int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            int lockCommand = intent.getIntExtra(Const.LOCK_ACTIVITY_COMMAND, 0);
            switch (lockCommand) {
                case Const.SHOW_LOCK_ACTIVITY:
                    runAsForeground();
                    break;
                case Const.HIDE_LOCK_ACTIVITY:
                    showCountDown();
                    break;
                case Const.CANCEL_LOCK_COUNTDOWN:
                    cancleCountdown();
                    break;
                case Const.STOP_SERVICE:
                    stopCountdown();
                    break;
                default:
                    break;
            }
        }


        return START_REDELIVER_INTENT;
    }

    private void cancleCountdown() {
        if (countDownTimer != null && countDownHead != null) {
            countDownHead.setVisibility(View.GONE);
            countDownTimer.cancel();
        }
    }

    public void showCountDown() {
        countDownTimer.start();
    }

    private void stopCountdown() {
        if (countDownTimer != null && countDownHead != null) {
            countDownHead.setVisibility(View.GONE);
            countDownTimer.cancel();
            this.stopSelf();
        }
    }


    private void preventSystemDialogs() {
        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        sendBroadcast(closeDialog);
    }


    private void runAsForeground() {
        Intent notificationIntent = new Intent(this, LockActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(notificationIntent);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(prefManager.getOwnerInfo()).build();

        startForeground(999, notification);
    }
}

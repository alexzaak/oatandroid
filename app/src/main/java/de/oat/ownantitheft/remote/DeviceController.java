package de.oat.ownantitheft.remote;

import android.annotation.TargetApi;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.device.DeviceStates;
import de.oat.ownantitheft.model.LocationData;
import de.oat.ownantitheft.model.UserNotify;
import de.oat.ownantitheft.network.RegisterGcmTask;
import de.oat.ownantitheft.network.ServerTask;
import de.oat.ownantitheft.receiver.AdminReceiver;
import de.oat.ownantitheft.services.AlarmService;
import de.oat.ownantitheft.services.EncryptService;
import de.oat.ownantitheft.services.LocationService;
import de.oat.ownantitheft.services.LockService;
import de.oat.ownantitheft.utils.AndroidVersion;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.ContactExtractor;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.PrefManager;
import de.oat.ownantitheft.utils.SystemUtils;

/**
 * @author Alexander Zaak
 *         19.08.15.
 */
public class DeviceController {

    private static final Logger LOGGER = Logger.getLogger(DeviceController.class);


    private static DevicePolicyManager policyManager = (DevicePolicyManager) AppController.getInstance().getSystemService(Context.DEVICE_POLICY_SERVICE);
    private static ComponentName componentName = AdminReceiver.getComponentName(AppController.getInstance());

    private static TelephonyManager telephoneMgr = (TelephonyManager) AppController.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
    private static AudioManager audioManager = (AudioManager) AppController.getInstance().getSystemService(Context.AUDIO_SERVICE);
    private static PrefManager prefManager = new PrefManager();

    private static Context context = AppController.getInstance();

    private DeviceController() {

    }

    public static ComponentName getComponentName() {
        return componentName;
    }


    /**
     * Method to start an alarm service
     *
     * @param duration duration in milliseconds
     */
    public static void startAlarm(int duration) {
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra(Const.ALARM_DURATION, duration);
        context.startService(intent);
    }

    /**
     * Method to detect whether the camera is enabled
     *
     * @return true if enabled, else false
     */
    public static boolean isCameraEnabled() {
        return !isAdminActive() || !policyManager.getCameraDisabled(componentName);
    }

    /**
     * Method to disabling the app as device admin
     */
    public static void disableDeviceAdmin() {
        policyManager.removeActiveAdmin(componentName);
    }

    /**
     * Method to lock the device an start the lock activity
     *
     * @param message     owner information, which will be displayed on screen
     * @param phoneNumber owner phone number
     * @param password    new keyguard password
     */
    public static void lockDevice(String message, String phoneNumber, String password) {

        prefManager.storeOwnerInfo(message);
        prefManager.storePhoneNumber(phoneNumber);
        prefManager.setLock(true);

        startLockService();
        setMasterSyncAuto(false);
        resetPassword(password);
        lockNow();
    }

    /**
     * Method to lock device and switch off devicce
     */
    public static void lockNow() {
        if (isAdminActive()) {
            policyManager.lockNow();
        }
    }

    /**
     * Method to starting the data encryption
     *
     * @param enabled true to encrypting, false to decrypting
     */
    public static void encryptDevice(boolean enabled) {
        Intent intent = new Intent(context, EncryptService.class);
        intent.putExtra(Const.ENCRYPTION, enabled);
        context.startService(intent);
    }

    /**
     * Method to send device status as SMS
     *
     * @param phoneNumber phone number
     * @throws UnsupportedEncodingException
     */
    public static void getStatus(String phoneNumber) throws UnsupportedEncodingException {
        DeviceStates device = new DeviceStates();
        sendData(device.getDeviceStates().toJson(), Const.METHOD_POST);
        if (phoneNumber != null) {
            SMSSender.sendSms(phoneNumber, device.getDeviceStates().toString());
        }
    }

    /**
     * Method to send device status to server
     *
     * @throws UnsupportedEncodingException
     */
    public static void getStatus() throws UnsupportedEncodingException {
        DeviceStates device = new DeviceStates();
        sendData(device.getDeviceStates().toJson(), Const.METHOD_POST);
    }

    /**
     * Method to disabling camera
     *
     * @param disable true to disable, false to enable
     */
    public static void disableCamera(boolean disable) {
        if (isAdminActive()) {
            policyManager.setCameraDisabled(componentName, disable);
        }
    }

    /**
     * Method to wipe device
     *
     * @param all true to wipe device completely, false to wipe only settings
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public static void wipePhone(boolean all) {
        try {
            if (isAdminActive()) {
                if (all && AndroidVersion.isLollipopMr1()) {
                    policyManager.wipeData(DevicePolicyManager.WIPE_RESET_PROTECTION_DATA);
                } else {
                    policyManager.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE);
                }
            }
        } catch (SecurityException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Method to start an outgoing call to the given phone number
     *
     * @param phoneNumber phone number for the outgoing call
     */
    public static void callMe(String phoneNumber) {
        if (!PermissionsCheck.hasPermissionCallPhone()) {
            LOGGER.error("Permission denied for Call");
            return;
        }

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("tel:" + phoneNumber));

        PackageManager packageManager = context.getPackageManager();
        List activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        for (int j = 0; j < activities.size(); j++) {
            if (activities.get(j).toString().toLowerCase().contains("com.android.phone")) {
                intent.setPackage("com.android.phone");
            } else if (activities.get(j).toString().toLowerCase().contains("call")) {
                LOGGER.debug(activities.get(j).toString());
                String pack = (activities.get(j).toString().split("[ ]")[1].split("[/]")[0]);
                LOGGER.debug(pack);
                intent.setPackage(pack);
            }
        }

        context.startActivity(intent);
    }

    /**
     * Method to store the sim card id at the first start
     */
    public static void storeSimcardID() {
        if (!PermissionsCheck.hasPermissionReadPhoneState()) {
            LOGGER.error("Permission denied for get Sim id, no permission read phone state");
            return;
        }

        int simState = telephoneMgr.getSimState();
        if (simState == TelephonyManager.SIM_STATE_UNKNOWN) {
            prefManager.storeSimcardId(null);
        } else {
            prefManager.storeSimcardId(telephoneMgr.getSubscriberId());
        }
    }

    /**
     * Method to detect whether the app is a device admin
     *
     * @return true, if device admin, else false
     */
    public static boolean isAdminActive() {
        return policyManager.isAdminActive(componentName);
    }

    /**
     * Method to setup in the preferences when the configuration was successful
     */
    public static void initSuccess() {
        prefManager.setPrefInitSuccess();
    }

    /**
     * Method to detect whether the configuration was successful
     *
     * @return true, if success, else false
     */
    public static boolean isInitSuccess() {
        return prefManager.prefsExists();
    }

    /**
     * Method to turn the speaker
     *
     * @param enabled true to turn the speaker loud, false to normal
     * @throws InterruptedException
     */
    public static void turnOnSpeaker(boolean enabled) throws InterruptedException {
        Thread.sleep(500);
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        audioManager.setSpeakerphoneOn(enabled);
    }

    /**
     * Method to send a request to the server and remove the device
     *
     * @throws UnsupportedEncodingException
     */
    public static void removeDevice(String prefKey) throws UnsupportedEncodingException {
        sendData(prefKey, Const.METHOD_DELETE);
    }

    /**
     * Method to send contacts to the server
     *
     * @param contacts contacts as byte code
     */
    public static void sendContacts(byte[] contacts) {
        ServerTask serverTask = new ServerTask(contacts, prefManager.getContactsApiAddress(), Const.METHOD_POST);
        serverTask.execute();
    }

    /**
     * Method to send a message to server
     *
     * @param userNotify message
     * @throws UnsupportedEncodingException
     */
    public static void sendMessage(UserNotify userNotify) throws UnsupportedEncodingException {
        ServerTask serverTask = new ServerTask(context, userNotify.toJson(), prefManager.getMessageApiAddress(), Const.METHOD_POST);
        serverTask.execute();
    }

    /**
     * Method to send location to the server
     *
     * @param locationData location data
     * @throws UnsupportedEncodingException
     */
    public static void sendLocation(LocationData locationData) throws UnsupportedEncodingException {
        ServerTask serverTask = new ServerTask(context, locationData.toJson(), prefManager.getLocationApiAddress(), Const.METHOD_POST);
        serverTask.execute();
    }

    /**
     * Method to register device on the gcm server
     */
    public static void registerGcm() {
        RegisterGcmTask registerGcmTask = new RegisterGcmTask(context);
        registerGcmTask.execute();
    }

    /**
     * Method to setup master sync auto
     *
     * @param enable true to enable, false to disable
     */
    protected static void setMasterSyncAuto(boolean enable) {
        ContentResolver.setMasterSyncAutomatically(enable);
    }

    /**
     * Method to reset the password
     *
     * @param password new password
     */
    protected static void resetPassword(String password) {
        if (isAdminActive()) {
            policyManager.setPasswordQuality(componentName, DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC);
            policyManager.resetPassword(password, DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
        }
    }

    /**
     * Method to setup secure settings
     *
     * @param isEnabled true to enable, false to disable
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected static void setSecureSettings(boolean isEnabled) {
        if (AndroidVersion.isLollipop()) {
            policyManager.setSecureSetting(componentName, Settings.Secure.LOCATION_MODE, isEnabled ? "1" : "0");
        }
    }

    /**
     * Method to setup global settings
     *
     * @param isEnabled true to enable, false to disable
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected static void setGlobalSetting(boolean isEnabled) {
        if (AndroidVersion.isLollipop()) {
            policyManager.setGlobalSetting(componentName, Settings.Global.ADB_ENABLED, isEnabled ? "1" : "0");
        }
    }

    /**
     * Method to detect whether the app is the device owner
     *
     * @return true, if device owner, else false
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected static boolean isDeviceOwnerApp() {
        if (AndroidVersion.isLollipop()) {
            return policyManager.isDeviceOwnerApp(context.getApplicationContext().getPackageName());
        }
        return false;
    }

    /**
     * Method to get contacts
     *
     * @throws IOException
     */
    protected static void getContacts() throws IOException {
        ContactExtractor.extractContacts(context);
    }

    /**
     * Method to toggle gps
     *
     * @param enable try to enable gps, false to disable gps
     * @throws SecurityException
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected static void toggleGps(boolean enable) throws SecurityException {
        if (SystemUtils.isSystemApp()) {
            if (AndroidVersion.isKitKat()) {
                ContentResolver cr = context.getContentResolver();
                int value;
                if (!enable) {
                    value = Settings.Secure.LOCATION_MODE_OFF;
                } else {
                    value = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
                }
                Settings.Secure.putInt(cr, Settings.Secure.LOCATION_MODE, value);
            } else {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", enable);
                context.sendBroadcast(intent);
            }
        }
    }

    /**
     * Method to toggle adb
     *
     * @param enable true to enable adb, false to disable adb
     * @throws SecurityException
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected static void toggleAdb(boolean enable) throws SecurityException {
        if (SystemUtils.isSystemApp()) {
            if (AndroidVersion.isKitKat()) {
                ContentResolver cr = context.getContentResolver();

                int value;
                if (!enable) {
                    value = 0;
                } else {
                    value = 1;
                }
                Settings.Global.putInt(cr, Settings.Global.ADB_ENABLED, value);
            }
        }
    }

    /**
     * Method to get current gps data
     *
     * @param enabled     true to enable the gps module and the gps service
     * @param duration    duration to sends gps data to server
     * @param phoneNumber phone number from owner, for sending data via sms
     * @throws SecurityException
     */
    protected static void getLocationByGps(boolean enabled, int duration, String phoneNumber) throws SecurityException {
        toggleGps(enabled);
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra(Const.GPS_DURATION, duration);
        intent.putExtra(Const.GPS_ENABLE, enabled);
        intent.putExtra(Const.GPS_PHONENUMBER, phoneNumber);
        context.startService(intent);
    }

    /**
     * Method to starting the lock service
     *
     * @see de.oat.ownantitheft.services.LockService
     */
    private static void startLockService() {
        Intent intent = new Intent(context, LockService.class);
        intent.putExtra(Const.LOCK_ACTIVITY_COMMAND, Const.SHOW_LOCK_ACTIVITY);
        context.startService(intent);
    }

    /**
     * Method to sending data to server
     *
     * @param json   data
     * @param method request method
     * @throws UnsupportedEncodingException
     */
    private static void sendData(String json, String method) throws UnsupportedEncodingException {
        ServerTask serverTask = new ServerTask(context, json, prefManager.getApiAddress(), method);
        serverTask.execute();
    }


}

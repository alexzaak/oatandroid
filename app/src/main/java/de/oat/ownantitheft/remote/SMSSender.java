package de.oat.ownantitheft.remote;

import android.telephony.SmsManager;

import org.apache.log4j.Logger;

import java.util.ArrayList;

import de.oat.ownantitheft.utils.PermissionsCheck;

/**
 * Class to sending sms to given phone number
 * @author Alexander Zaak
 *         12.09.15.
 */
public class SMSSender {
    private static final Logger LOGGER = Logger.getLogger(SMSSender.class);
    private static SmsManager sms = SmsManager.getDefault();

    private SMSSender() {

    }

    /**
     * Method to sending sms to given phone number
     *
     * @param phoneNumber phone number
     * @param message     message
     */
    public static void sendSms(String phoneNumber, String message) {
        if (!PermissionsCheck.hasPermissionSendSMS()) {
            LOGGER.error("permission denied for send sms");
            return;
        }
        ArrayList<String> parts = sms.divideMessage(message);
        sms.sendMultipartTextMessage(phoneNumber, null, parts, null, null);

    }
}

package de.oat.ownantitheft.remote;

import org.apache.log4j.Logger;

import java.io.IOException;

import de.oat.ownantitheft.device.DeviceStates;
import de.oat.ownantitheft.model.OATCommands;

import static de.oat.ownantitheft.utils.Commands.ALARM;
import static de.oat.ownantitheft.utils.Commands.CALL_ME;
import static de.oat.ownantitheft.utils.Commands.DISABLE_CAMERA;
import static de.oat.ownantitheft.utils.Commands.ENABLE_BT;
import static de.oat.ownantitheft.utils.Commands.ENABLE_GPS;
import static de.oat.ownantitheft.utils.Commands.ENABLE_MASTER_SYNC;
import static de.oat.ownantitheft.utils.Commands.ENABLE_WIFI;
import static de.oat.ownantitheft.utils.Commands.ENCRYPT_STORAGE;
import static de.oat.ownantitheft.utils.Commands.GET_CONTACTS;
import static de.oat.ownantitheft.utils.Commands.LOCALE_WITH_GPS;
import static de.oat.ownantitheft.utils.Commands.LOCK;
import static de.oat.ownantitheft.utils.Commands.RESET_PASSWORD;
import static de.oat.ownantitheft.utils.Commands.SET_BRIGHTNESS;
import static de.oat.ownantitheft.utils.Commands.SET_GLOBAL_SETTINGS;
import static de.oat.ownantitheft.utils.Commands.SET_SECURE_SETTINGS;
import static de.oat.ownantitheft.utils.Commands.STATUS;
import static de.oat.ownantitheft.utils.Commands.WIPE;

/**
 * Method to manage all incoming commands
 *
 * @author Alexander Zaak
 *         15.08.15.
 */
public class OATSubCommander {

    private static final Logger LOGGER = Logger.getLogger(OATSubCommander.class);

    /**
     * Method controlDevice the device by received commands
     *
     * @param OATCommands received command
     * @param phoneNumber phone number
     */
    public void controlDevice(OATCommands OATCommands, String phoneNumber) {
        try {
            switch (OATCommands.getCno()) {
                case STATUS:
                    DeviceController.getStatus(phoneNumber);
                    break;
                case LOCK:
                    DeviceController.lockDevice(OATCommands.getMessage(), OATCommands.getPhoneNumber(), OATCommands.getValue());
                    break;
                case ALARM:
                    DeviceController.startAlarm(OATCommands.getDuration());
                    break;
                case RESET_PASSWORD:
                    DeviceController.resetPassword(OATCommands.getValue());
                    break;
                case ENCRYPT_STORAGE:
                    DeviceController.encryptDevice(OATCommands.isEnabled());
                    break;
                case SET_SECURE_SETTINGS:
                    if (DeviceController.isDeviceOwnerApp()) {
                        DeviceController.setSecureSettings(OATCommands.isEnabled());
                    }
                    break;
                case SET_GLOBAL_SETTINGS:
                    if (DeviceController.isDeviceOwnerApp()) {
                        DeviceController.setGlobalSetting(OATCommands.isEnabled());
                    }
                    break;
                case DISABLE_CAMERA:
                    DeviceController.disableCamera(!OATCommands.isEnabled());
                    break;
                case ENABLE_MASTER_SYNC:
                    DeviceController.setMasterSyncAuto(OATCommands.isEnabled());
                    break;
                case WIPE:
                    DeviceController.wipePhone(OATCommands.isAll());
                    break;
                case ENABLE_WIFI:
                    DeviceStates.enableWifi(OATCommands.isEnabled());
                    break;
                case ENABLE_BT:
                    DeviceStates.enableBt(OATCommands.isEnabled());
                    break;
                case ENABLE_GPS:
                    DeviceController.toggleGps(OATCommands.isEnabled());
                    break;
                case SET_BRIGHTNESS:
                    DeviceStates.setBrightness(OATCommands.getLevel());
                    break;
                case LOCALE_WITH_GPS:
                    DeviceController.getLocationByGps(OATCommands.isEnabled(), OATCommands.getDuration(), phoneNumber);
                    break;
                case GET_CONTACTS:
                    DeviceController.getContacts();
                    break;
                case CALL_ME:
                    DeviceController.callMe(OATCommands.getPhoneNumber());
                    DeviceController.turnOnSpeaker(true);
                    break;
                default:
                    LOGGER.error("wrong command " + OATCommands.getCno());
                    break;
            }

        } catch (IOException | InterruptedException e) {
            LOGGER.error(e);
        }
    }


}

package de.oat.ownantitheft;

import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import net.xpece.android.support.preference.Fixes;

import org.apache.log4j.Logger;

import java.io.File;

import de.oat.ownantitheft.device.DeviceStates;
import de.oat.ownantitheft.network.InitialDataTask;
import de.oat.ownantitheft.network.RootTask;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PermissionsCheck;

/**
 * Activity for loading fragments
 *
 * @author Alexander Zaak
 *         08.12.15
 * @since 1.0.1.3 Alpha
 */
public class SettingsActivity extends AppCompatActivity implements SettingsFragment.OnFragmentInteractionListener, UninstallFragment.OnFragmentInteractionListener {
    /**
     * Custom logger to log error and debug output to LogCat an logfile
     */
    private static final Logger LOGGER = Logger.getLogger(SettingsActivity.class);
    private SettingsFragment mSettingsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fixes.updateLayoutInflaterFactory(getLayoutInflater());
        setContentView(R.layout.main_layout);
        Intent intent = getIntent();

        String fragmentTag = intent.getStringExtra(Const.FRAGMENT_TAG);
        if (fragmentTag != null) {
            addFragment(savedInstanceState, fragmentTag);
        }

        setConfiguration(intent, savedInstanceState);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDeviceAdminEnabled(boolean enabled, int requestCode) {
        if (enabled) {
            // Launch the activity to have the user enable our admin.
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, DeviceController.getComponentName());
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                    this.getString(R.string.admin_description));
            this.startActivityForResult(intent, requestCode);
        } else {
            DeviceController.disableDeviceAdmin();
        }
    }


    @Override
    public void onSystemAppEnabled(boolean enabled) {
        RootTask rootTask = new RootTask(this, enabled);
        rootTask.execute();
    }

    @Override
    public void onAllPermissionsEnabled(boolean enabled) {

        requestForPermissions(Const.REQUIRED_PERMISSIONS, enabled);

    }

    @Override
    public void onContactPermissionEnabled(boolean enabled) {
        requestForPermissions(Const.CONTACT_PERMISSIONS, enabled);

    }

    @Override
    public void onSmsPermissionEnabled(boolean enabled) {
        requestForPermissions(Const.SMS_PERMISSIONS, enabled);
    }

    @Override
    public void onStoragePermissionEnabled(boolean enabled) {
        requestForPermissions(Const.STORAGE_PERMISSIONS, enabled);
    }

    @Override
    public void onLocationEnabled(boolean enabled) {
        requestForPermissions(Const.LOCATION_PERMISSIONS, enabled);
    }

    @Override
    public void onTelephonyEnabled(boolean enabled) {
        requestForPermissions(Const.TELEPHONY_PERMISSIONS, enabled);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        boolean adminEnabled = DeviceController.isAdminActive();
        switch (requestCode) {
            case Const.REQUEST_CODE_DEVICE_ADMIN_SETTINGS_FRG:
                mSettingsFragment.setDeviceAdminCheckbox(adminEnabled);
                break;

        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Const.PERMISSIONS_REQUEST_CODE) {
            mSettingsFragment.isPermissionEnabled();
        }
    }

    private void addFragment(Bundle savedInstanceState, String fragmentTag) {
        UninstallFragment mUninstallFragment;
        if (savedInstanceState == null) {
            switch (fragmentTag) {
                case Const.FRAGMENT_SETTINGS:
                    mSettingsFragment = new SettingsFragment();
                    getSupportFragmentManager().beginTransaction().add(R.id.content, mSettingsFragment, fragmentTag).commit();
                    break;

                case Const.FRAGMENT_UNINSTALL:
                    mUninstallFragment = new UninstallFragment();
                    getSupportFragmentManager().beginTransaction().add(R.id.content, mUninstallFragment, fragmentTag).commit();
                    break;
            }


        } else {
            switch (fragmentTag) {
                case Const.FRAGMENT_SETTINGS:
                    mSettingsFragment = (SettingsFragment) getSupportFragmentManager().findFragmentByTag(fragmentTag);
                    break;

                case Const.FRAGMENT_UNINSTALL:
                    mUninstallFragment = (UninstallFragment) getSupportFragmentManager().findFragmentByTag(fragmentTag);
                    break;
            }

        }
    }

    private void requestForPermissions(String[] permissionsSummary, boolean enabled) {
        if (enabled) {
            String[] permissions = PermissionsCheck.checkRequiredPermissions(permissionsSummary);
            PermissionsCheck.requestPermissions(this, permissions);
        } else {
            showToast(getString(R.string.permissions_warning_summary));
            mSettingsFragment.isPermissionEnabled();
        }
    }

    private void setConfiguration(Intent intent, Bundle savedInstanceState) {
        if (checkPlayServices()) {
            // Get the intent that started this activity

            Uri data = intent.getData();
            LOGGER.debug(data);
            if (data == null)
                return;

            if (data.getPath().equals(File.separator + DeviceStates.getAndroidId() + Const.API_REMOVE_ROUTE)) {
                addFragment(savedInstanceState, Const.FRAGMENT_UNINSTALL);
                return;
            }
            addFragment(savedInstanceState, Const.FRAGMENT_SETTINGS);
            String param = data.getQueryParameter(Const.CONF_PARAM);
            showToast(this.getString(R.string.device_configuration_info));
            InitialDataTask initialDataTask = new InitialDataTask(this, param);
            initialDataTask.execute();

        }
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        Const.PLAY_SERVICES_RESOLUTION_REQUEST);
                errorDialog.setCancelable(false);
                errorDialog.show();
            } else {
                showToast(getString(R.string.device_not_supported));
            }
            return false;
        }
        return true;
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(),
                message, Toast.LENGTH_LONG)
                .show();
    }


}

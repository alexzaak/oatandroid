package de.oat.ownantitheft.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;

import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;

import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class to interact with the server
 *
 * @author Alexander Zaak
 *         11.08.15
 */
public class ServerTask extends AsyncTask<Void, Void, Boolean> {

    private static final Logger LOGGER = Logger.getLogger(ServerTask.class);
    private byte[] data = null;
    private String contentType;
    private String apiUrl;
    private Context context;
    private String method;

    public ServerTask(Context context, String json, String url, String method) throws UnsupportedEncodingException {

        this.contentType = "application/json;charset=utf-8";
        this.data = json.getBytes(Const.ENCODING_UTF_8);
        this.apiUrl = url;
        this.context = context;
        this.method = method;
        LOGGER.debug(json);
    }

    public ServerTask(byte[] contacts, String url, String method) {
        this.contentType = "text/x-vcard;charset=utf-8";
        this.data = contacts;
        this.apiUrl = url;
        this.method = method;
    }

    /**
     * Method to sending request to the server
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        try {

            URL url = new URL(apiUrl);

            HttpURLConnection urlConnection;


            if (url.getProtocol().startsWith("https")) {
                urlConnection = (HttpsURLConnection) url.openConnection();
            } else {
                urlConnection = (HttpURLConnection) url.openConnection();
            }

            if (urlConnection == null) {
                LOGGER.error(Const.ERROR_NOTNULL);
                return false;
            } else {

                urlConnection.setConnectTimeout(5000);
                urlConnection.setRequestMethod(this.method);
                urlConnection.setRequestProperty("Content-Type", this.contentType);
                urlConnection.setRequestProperty("Authorization", "Basic " + authorization());


                if (!this.method.equals(Const.METHOD_DELETE)) {
                    urlConnection.setDoOutput(true); // Triggers POST.
                    urlConnection.setRequestProperty("Accept-Charset", Const.ENCODING_UTF_8);
                    urlConnection.setRequestProperty("Accept", "application/json");

                    OutputStream output = urlConnection.getOutputStream();
                    output.write(this.data);
                }

                urlConnection.connect();

                int responseCode = urlConnection.getResponseCode();
                if (responseCode == 200 || responseCode == 201) {
                    InputStream instr = urlConnection.getInputStream();
                    readResponse(instr);
                    return true;
                } else {
                    InputStream instr = urlConnection.getErrorStream();
                    readResponse(instr);
                    return false;
                }
            }


        } catch (SocketTimeoutException e) {

            LOGGER.error(Const.ERROR_BACKGROUND_TASK + e);

            return false;

        } catch (JSONException | ClassCastException | IOException e) {

            LOGGER.error(Const.ERROR_BACKGROUND_TASK + e);

            return false;

        }
    }

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {
        sendBroadcast();
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param result The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Boolean result) {
        try {
            sendBroadcast(result);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Method to send broadcasts
     */
    private void sendBroadcast(boolean registered) throws UnsupportedEncodingException {
        Intent i = new Intent();
        if (this.method.equals(Const.METHOD_DELETE)) {
            i.setAction(Const.OAT_UNINSTALL_BROADCAST);
            i.putExtra(Const.PREF_KEY, new String(this.data, Const.ENCODING_UTF_8));
        } else {
            i.setAction(Const.DEVICE_TASK_BROADCAST);
            i.putExtra(Const.IS_DEVICE_REGISTERED, registered);
        }
        if (this.context != null) {

            this.context.sendBroadcast(i);
        }
    }


    /**
     * Method to send broadcasts
     */
    private void sendBroadcast() {
        if (this.context != null) {
            Intent i = new Intent(Const.DEVICE_TASK_PROGRESS);
            this.context.sendBroadcast(i);
        }
    }


    /**
     * Method to create Basic authorization
     *
     * @return encoded basic authorization
     * @throws UnsupportedEncodingException
     */

    private String authorization() throws UnsupportedEncodingException {
        PrefManager prefManager = new PrefManager();
        String username = prefManager.getEmail();
        String password = prefManager.getPassword();
        byte[] credentials =
                (username + ":" + password).getBytes(Charset.forName(Const.ENCODING_UTF_8));
        return Base64.encodeToString(credentials, Base64.NO_WRAP);
    }

    /**
     * Method to store Data from Server in the preferences
     * <p/>
     *
     * @param instr inputStream of Data
     * @throws JSONException
     * @throws IOException
     */

    private void readResponse(InputStream instr) throws JSONException, IOException {
        if (instr == null) {
            return;
        }
        StringBuilder responseMessage = new StringBuilder();
        int i;
        while ((i = instr.read()) != -1) {
            responseMessage.append((char) i);
        }
        if (!responseMessage.toString().isEmpty()) {
            LOGGER.error("Content " + responseMessage.toString());
        }
    }
}

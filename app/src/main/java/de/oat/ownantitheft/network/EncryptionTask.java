package de.oat.ownantitheft.network;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import de.oat.ownantitheft.device.DeviceStates;
import de.oat.ownantitheft.utils.AndroidVersion;
import de.oat.ownantitheft.utils.Const;

/**
 * AsyncTask to encrypt and decrypt files in following folders:
 * DCIM, Documents, Downloads, Pictures and movies
 *
 * @author Alexander Zaak
 *         10.01.16.
 */
public class EncryptionTask extends AsyncTask<Void, Void, Void> {

    private static final Logger LOGGER = Logger.getLogger(EncryptionTask.class);
    private Context context;
    private boolean encrypt;

    public EncryptionTask(Context context, boolean encrypt) {
        this.context = context;
        this.encrypt = encrypt;
    }

    /**
     * Method to perform the encryption or decryption of files by an extra thread
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     */
    @Override
    protected Void doInBackground(Void... params) {
        ArrayList<String> filesToEncrypt = getFilesToEncrypt();

        for (String filePath : filesToEncrypt) {

            File file = new File(filePath);
            try {
                if (this.encrypt) {
                    encrypt(file);
                } else {
                    decrypt(file);
                }
            } catch (IOException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
                LOGGER.error(e);
            }
        }
        return null;
    }


    /**
     * Method to encrypt a file
     *
     * @param file File to encrypt
     * @throws IOException              when file isn't readable
     * @throws NoSuchAlgorithmException when the used algorithm isn't defined
     * @throws NoSuchPaddingException   when the defined padding isn't valid
     * @throws InvalidKeyException      when the defined key isn't valid
     */
    private void encrypt(File file) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {

        if (this.encrypt && file.getName().endsWith(".encoat")) {
            return;
        }

        FileInputStream fis = new FileInputStream(file.getAbsolutePath());
        // This stream write the encrypted text. This stream will be wrapped by another stream.
        FileOutputStream fos = new FileOutputStream(file.getAbsolutePath() + ".encoat");

        // Length is 16 byte
        SecretKeySpec sks = new SecretKeySpec(DeviceStates.getAndroidId().getBytes(), "AES");
        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[8];
        while ((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();

        if (!file.delete()) {
            LOGGER.error("Delete operation failed");
        }
    }

    /**
     * Method to decrypt a file
     *
     * @param file File to decrypt
     * @throws IOException              when file isn't readable
     * @throws NoSuchAlgorithmException when the used algorithm isn't defined
     * @throws NoSuchPaddingException   when the defined padding isn't valid
     * @throws InvalidKeyException      when the defined key isn't valid
     */
    private void decrypt(File file) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {

        if (this.encrypt && !file.getName().endsWith(".encoat")) {
            return;
        }
        FileInputStream fis = new FileInputStream(file.getAbsolutePath());

        FileOutputStream fos = new FileOutputStream(getFileName(file));
        SecretKeySpec sks = new SecretKeySpec(DeviceStates.getAndroidId().getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[8];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();

        if (!file.delete()) {
            LOGGER.error("Delete operation failed");
        }

    }

    /**
     * Called when Encryption is finished
     *
     * @param result result from doBackground() Method
     * @see #doInBackground(Void...)
     */
    @Override
    protected void onPostExecute(Void result) {
        sendBroadcast();
    }


    /**
     * Method to send broadcasts
     */
    private void sendBroadcast() {
        Intent i = new Intent(Const.ENCRYPTION_FINISHED);
        this.context.sendBroadcast(i);
    }

    /**
     * Method to get the original file name of the encrypted files
     *
     * @param file encrypted file
     * @return filename
     */
    private String getFileName(File file) {
        String fileName;
        int lastIndexOfEnc = file.getAbsolutePath().lastIndexOf(".encoat");
        fileName = file.getAbsolutePath().substring(0, lastIndexOfEnc);
        return fileName;
    }

    /**
     * Method to get all files that have to be encrypted
     *
     * @return List of files
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private ArrayList<String> getFilesToEncrypt() {
        ArrayList<String> fileList = new ArrayList<>();
        String[] dirs;

        if (AndroidVersion.isKitKat()) {
            dirs = new String[]{Environment.DIRECTORY_DCIM, Environment.DIRECTORY_DOCUMENTS, Environment.DIRECTORY_DOWNLOADS, Environment.DIRECTORY_PICTURES, Environment.DIRECTORY_MOVIES};
        } else {
            dirs = new String[]{Environment.DIRECTORY_DCIM, Environment.DIRECTORY_DOWNLOADS, Environment.DIRECTORY_PICTURES, Environment.DIRECTORY_MOVIES};
        }

        for (String dir : dirs) {
            File files[] = Environment.getExternalStoragePublicDirectory(dir).listFiles();
            recursiveFileFind(files, fileList);
        }
        return fileList;
    }

    /**
     * Method to find recursively all files that have to be encrypted
     *
     * @param file1     array of files
     * @param filesList List of files
     */
    private void recursiveFileFind(File[] file1, ArrayList<String> filesList) {
        int i = 0;
        if (file1 != null) {
            while (i != file1.length) {
                String filePath = file1[i].getAbsolutePath();
                if (file1[i].isDirectory()) {
                    File file[] = file1[i].listFiles();
                    recursiveFileFind(file, filesList);
                } else {
                    filesList.add(filePath);
                }
                i++;
            }
        }
    }
}

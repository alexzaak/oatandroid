package de.oat.ownantitheft.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.SystemUtils;

/**
 * Class for root operations
 *
 * @author Alexander Zaak
 *         24.09.15
 */
public class RootTask extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private boolean enabled;

    public RootTask(Context context, boolean enabled) {
        this.context = context;
        this.enabled = enabled;
    }

    /**
     * Method to register OwnAntiTheft as a System-App
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        return SystemUtils.setAsSystemApp(this.enabled);
    }


    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {

    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param result The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Boolean result) {
        sendBroadcast(result);
    }

    /**
     * Method to send broadcasts
     */
    private void sendBroadcast(boolean registered) {
        Intent i = new Intent(Const.SYSTEM_APP_TASK_BROADCAST);
        i.putExtra(Const.IS_SYSTEM_APP_REGISTERED, registered);
        this.context.sendBroadcast(i);
    }
}

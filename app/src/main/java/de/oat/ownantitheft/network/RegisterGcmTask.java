package de.oat.ownantitheft.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.log4j.Logger;

import java.io.IOException;

import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class to register device for google cloud messaging
 *
 * @author Alexander Zaak
 *         17.08.15
 */
public class RegisterGcmTask extends AsyncTask<Void, Void, Boolean> {
    private static final Logger LOGGER = Logger.getLogger(ServerTask.class);
    private Context context;
    private GoogleCloudMessaging googleCloudMessaging;
    private PrefManager prefManager;


    public RegisterGcmTask(Context context) {
        this.context = context;
        this.prefManager = new PrefManager();

    }

    /**
     * Method to perform the google cloud messaging registration
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see <a href="https://developers.google.com/cloud-messaging/">GoogleCloudMessaging</a>
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            if (googleCloudMessaging == null) {
                googleCloudMessaging = GoogleCloudMessaging.getInstance(context);

                String regId = googleCloudMessaging.register(prefManager.getSenderId());
                prefManager.storeGcmRegId(regId);
            }
        } catch (IOException e) {

            LOGGER.error(e);

            return false;
        }

        return true;
    }

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {
        sendBroadcast();
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param result The result of the operation computed by {@link #doInBackground}.
     */
    @Override
    protected void onPostExecute(Boolean result) {
        sendBroadcast(result);
    }

    /**
     * Method to send broadcasts
     */
    private void sendBroadcast(boolean registered) {
        Intent i = new Intent(Const.GCM_TASK_BROADCAST);
        i.putExtra(Const.IS_GCM_REGISTERED, registered);
        this.context.sendBroadcast(i);
    }

    /**
     * Method to send broadcasts
     */
    private void sendBroadcast() {
        Intent i = new Intent(Const.GCM_TASK_PROGRESS);
        this.context.sendBroadcast(i);
    }

}

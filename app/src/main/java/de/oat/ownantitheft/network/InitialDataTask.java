package de.oat.ownantitheft.network;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;

import com.google.gson.Gson;

import org.apache.log4j.Logger;

import de.oat.ownantitheft.model.Config;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * @author Alexander Zaak
 *         11.01.16.
 */
public class InitialDataTask extends AsyncTask<Void, Void, Void> {

    private static final Logger LOGGER = Logger.getLogger(InitialDataTask.class);
    private String data;
    private PrefManager prefManager;
    private Context context;

    public InitialDataTask(Context context, String data) {
        this.data = data;
        this.context = context;
    }

    /**
     * Method to decode configuration data and store the data in the preferences
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Void doInBackground(Void... params) {
        this.prefManager = new PrefManager();

        LOGGER.debug(data);
        base64UrlDecode(data);

        return null;
    }

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {
        Intent i = new Intent(Const.DATA_TASK_PROGRESS);
        this.context.sendBroadcast(i);
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param aVoid The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        Intent i = new Intent(Const.DATA_TASK_BROADCAST);
        i.putExtra(Const.IS_DEVICE_REGISTERED, true);
        this.context.sendBroadcast(i);
    }

    /**
     * Method to decode a Base64UrlEncode
     *
     * @param input Input of base64 encoded configuration
     */
    private void base64UrlDecode(String input) {
        if (input != null && !input.isEmpty()) {
            byte[] decodedBytes = Base64.decode(input, Base64.URL_SAFE);
            storeConfig(new String(decodedBytes));
        }
    }

    /**
     * Method to store configurations to preferences
     *
     * @param json Json with configuration data
     */
    private void storeConfig(String json) {
        Gson gson = new Gson();
        Config conf = gson.fromJson(json, Config.class);
        prefManager.storeHostAddress(conf.getHostApi());
        prefManager.storeEmail(conf.getEmail());
        prefManager.storePassword(conf.getPass());
        prefManager.storeSenderId(conf.getSenderId());

    }


}

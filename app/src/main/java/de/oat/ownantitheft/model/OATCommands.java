package de.oat.ownantitheft.model;

import android.content.Context;
import android.os.Bundle;

import java.util.regex.Matcher;

import de.oat.ownantitheft.R;
import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.remote.SMSSender;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PasswordGenerator;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class for commands
 *
 * @author Alexander Zaak
 *         15.08.15
 */
public class OATCommands {

    private int cno = -1;
    private String message;
    private String phoneNumber;
    private int duration;
    private String value;
    private boolean enabled;
    private boolean all;
    private int level;


    public OATCommands(int cno) {
        this.cno = cno;
    }

    public OATCommands() {
    }

    public OATCommands(Matcher matcher, String phoneNumber) {
        Context context = AppController.getInstance();

        if (matcher.group(2) != null && matcher.group(2).equals(de.oat.ownantitheft.utils.Commands.SMS_CMD_LOCK)) {

            if (!isSmsCommandVerified(matcher.group(1))) {
                return;
            }
            this.cno = de.oat.ownantitheft.utils.Commands.LOCK;
            this.message = matcher.group(3);
            this.phoneNumber = phoneNumber;
            this.value = PasswordGenerator.generatePassword();

            SMSSender.sendSms(phoneNumber, String.format(context.getString(R.string.lockscreen_password), this.value));
        }

        if (matcher.group(5) != null) {
            String command = matcher.group(5).toLowerCase();
            if (!isSmsCommandVerified(matcher.group(4))) {
                return;
            }
            switch (command) {
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_STATUS:
                    this.cno = de.oat.ownantitheft.utils.Commands.STATUS;
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_CONTACTS:
                    this.cno = de.oat.ownantitheft.utils.Commands.GET_CONTACTS;
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_CALLBACK:
                    this.cno = de.oat.ownantitheft.utils.Commands.CALL_ME;
                    this.phoneNumber = phoneNumber;
                    break;

            }
        }

        if (matcher.group(7) != null) {
            String command = matcher.group(7).toLowerCase();
            String attribute = matcher.group(8);
            if (!isSmsCommandVerified(matcher.group(6))) {
                return;
            }
            switch (command) {
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_ALARM:
                    this.cno = de.oat.ownantitheft.utils.Commands.ALARM;
                    this.duration = toInt(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_ENCRYPT:
                    this.cno = de.oat.ownantitheft.utils.Commands.ENCRYPT_STORAGE;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_CAMERA:
                    this.cno = de.oat.ownantitheft.utils.Commands.DISABLE_CAMERA;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_SYNC:
                    this.cno = de.oat.ownantitheft.utils.Commands.ENABLE_MASTER_SYNC;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_WIFI:
                    this.cno = de.oat.ownantitheft.utils.Commands.ENABLE_WIFI;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_BT:
                    this.cno = de.oat.ownantitheft.utils.Commands.ENABLE_BT;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_GPS:
                    this.cno = de.oat.ownantitheft.utils.Commands.ENABLE_GPS;
                    this.enabled = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_BRIGHT:
                    this.cno = de.oat.ownantitheft.utils.Commands.SET_BRIGHTNESS;
                    this.level = toInt(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_LOCATE:
                    this.cno = de.oat.ownantitheft.utils.Commands.LOCALE_WITH_GPS;
                    this.enabled = true;
                    this.duration = toInt(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_WIPE:
                    this.cno = de.oat.ownantitheft.utils.Commands.WIPE;
                    this.all = toBool(attribute);
                    break;
                case de.oat.ownantitheft.utils.Commands.SMS_CMD_SECURE:
                    this.cno = de.oat.ownantitheft.utils.Commands.SET_SECURE_SETTINGS;
                    this.enabled = toBool(attribute);
                    break;

            }
        }
    }

    /**
     * @param extras
     * @throws NumberFormatException
     */
    public OATCommands(Bundle extras) throws NumberFormatException {
        this.cno = toInt(extras.getString(Const.GCM_CNO));
        this.message = extras.getString(Const.GCM_MSG);
        this.phoneNumber = extras.getString(Const.GCM_PHONE_NO);
        this.duration = toInt(extras.getString(Const.GCM_DURATION));
        this.value = extras.getString(Const.GCM_VALUE);
        this.enabled = toBool(extras.getString(Const.GCM_ENABLED));
        this.all = toBool(extras.getString(Const.GCM_ALL));
        this.level = toInt(extras.getString(Const.GCM_LEVEL));
    }



    public int getCno() {
        return cno;
    }

    public String getMessage() {
        return message;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getDuration() {
        return duration;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isAll() {
        return all;
    }

    public int getLevel() {
        return level;
    }

    /**
     *
     * @param password
     * @return
     */
    private boolean isSmsCommandVerified(String password) {
        PrefManager prefManager = new PrefManager();
        String storedPassword = prefManager.getPassword();
        if (password == null) {
            return false;
        }
        return storedPassword == null || storedPassword.equals(password);
    }

    /**
     * @param value
     * @return
     */
    private boolean toBool(String value) {
        if (value == null) {
            return false;
        }
        return "true".equals(value) || "1".equals(value);
    }

    /**
     * @param value
     * @return
     * @throws NumberFormatException
     */
    private int toInt(String value) throws NumberFormatException {
        if (value == null) {
            return -1;
        }
        return Integer.valueOf(value);
    }


}

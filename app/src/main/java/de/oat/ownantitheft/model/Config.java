package de.oat.ownantitheft.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Class to parse config
 *
 * @author Alexander Zaak
 * 19.08.15
 */
public class Config {
    @SerializedName("api_url")
    private String hostApi;
    @SerializedName("password")
    private String pass;
    @SerializedName("email")
    private String email;
    @SerializedName("sender_id")
    private String senderId;

    public String getHostApi() {
        return hostApi;
    }

    public String getPass() {
        return pass;
    }

    public String getEmail() {
        return email;
    }

    public String getSenderId() {
        return senderId;
    }

    /**
     * @return
     */
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}

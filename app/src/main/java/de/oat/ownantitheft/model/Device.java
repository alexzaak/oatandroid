package de.oat.ownantitheft.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Class for device information
 *
 * @author Alexander Zaak
 * 10.08.15
 */
public class Device {
    private String name;
    private String imei;
    @SerializedName("google_reg")
    private String regId;
    @SerializedName("battery_level")
    private int batteryLevel;
    @SerializedName("cell_info")
    private CellLocationInfo cellInfo;
    private DeviceSettings settings;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }


    public void setRegId(String regId) {
        this.regId = regId;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }


    public void setCellInfo(CellLocationInfo cellInfo) {
        this.cellInfo = cellInfo;
    }


    public void setSettings(DeviceSettings settings) {
        this.settings = settings;
    }


    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return String.format("IMEI: %s\nAkku: %d%%\n%s\n%s", imei, batteryLevel, cellInfo.toString(), settings.toString());
    }

}

package de.oat.ownantitheft.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for device settings
 *
 * @author Alexander Zaak
 * 11.08.15
 */
public class DeviceSettings {
    private boolean wifi;
    private boolean gps;
    private boolean bt;
    private int brightness;
    @SerializedName("network_type")
    private int networkType;
    private boolean camera;
    private boolean sync;
    private boolean locked;
    @SerializedName("system_app")
    private boolean isSystemApp;
    @SerializedName("device_admin")
    private boolean isAdminEnabled;

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public void setGps(boolean gps) {
        this.gps = gps;
    }

    public void setBt(boolean bt) {
        this.bt = bt;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public void setNetworkType(int networkType) {
        this.networkType = networkType;
    }

    public void setCamera(boolean camera) {
        this.camera = camera;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }


    public void setIsSystemApp(boolean isSystemApp) {
        this.isSystemApp = isSystemApp;
    }

    public void setIsAdminEnabled(boolean isAdminEnabled) {
        this.isAdminEnabled = isAdminEnabled;
    }

    @Override
    public String toString() {
        return String.format("WIFI: %b\nGPS: %b\nBT: %b\nBRIGHTNESS: %d\n" +
                "CELL: %d\n" +
                "CAMERA: %b\n" +
                "SYNC: %b\n" +
                "LOCKED: %b\n" +
                "SYSTEMAPP: %b\n" +
                "DEVICEADMIN: %b", wifi, gps, bt, brightness, networkType, camera, sync, locked, isSystemApp, isAdminEnabled);
    }
}

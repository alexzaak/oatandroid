package de.oat.ownantitheft.model;

/**
 * Class for cell information
 *
 * @author Alexander Zaak
 * 11.08.15
 */
public class CellLocationInfo {
    private int lac;
    private int cid;
    private int mcc;
    private int mnc;

    public void setLac(int lac) {
        this.lac = lac;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }


    @Override
    public String toString() {
        return String.format("LAC: %d\nCID: %d\nMCC: %d\nMNC: %d", lac, cid, mcc, mnc);
    }
}

package de.oat.ownantitheft.model;

import com.google.gson.Gson;

/**
 * Class for user notifications
 *
 * @author Alexander Zaak
 *         20.08.15
 */
public class UserNotify {
    private String subject;
    private String message;

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}

package de.oat.ownantitheft.model;

import com.google.gson.Gson;

/**
 * Class for location data
 *
 * @author Alexander Zaak
 * 18.08.15
 */
public class LocationData {
    private double lat;
    private double lng;
    private int accuracy;


    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return String.format("https://maps.google.com/maps?q=%f,%f\nAccuracy: %d", lat, lng, accuracy);
    }
}

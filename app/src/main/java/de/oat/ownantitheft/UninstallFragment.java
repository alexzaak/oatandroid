package de.oat.ownantitheft;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.XpPreferenceFragment;
import android.support.v7.widget.PreferenceDividerDecoration;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import net.xpece.android.support.preference.PreferenceScreenNavigationStrategy;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.receiver.UninstallReceiver;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.SystemUtils;

/**
 * @author Alexander Zaak
 *         08.12.2015
 */
public class UninstallFragment extends XpPreferenceFragment implements
        PreferenceScreenNavigationStrategy.Callbacks, UninstallReceiver.OnUninstallTaskInteractionListener {


    private static final Logger LOGGER = Logger.getLogger(UninstallFragment.class);

    private OnFragmentInteractionListener mListener;
    // These are used to navigate back and forth between subscreens.
    private PreferenceScreenNavigationStrategy mPreferenceScreenNavigation;

    private UninstallReceiver uninstallReceiver;

    @Override
    public void onCreatePreferences2(final Bundle savedInstanceState, final String rootKey) {
        // Add 'general' preferences.

        addPreferencesFromResource(R.xml.pref_uninstall);

        Preference uninstallPref = findPreference(Const.KEY_OAT_UNINSTALL);

        Preference uninstallSystemAppPref = findPreference(Const.KEY_SYSTEM_APP_UNINSTALL);

        uninstallPref.setVisible(!SystemUtils.isSystemApp());
        uninstallSystemAppPref.setVisible(SystemUtils.isSystemApp());

        uninstallPref.setOnPreferenceClickListener(createOnPreferenceClickListener());
        uninstallSystemAppPref.setOnPreferenceClickListener(createOnPreferenceClickListener());

        mPreferenceScreenNavigation = new PreferenceScreenNavigationStrategy.ReplaceRoot(this, this);
        mPreferenceScreenNavigation.onCreate(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mPreferenceScreenNavigation.onSaveInstanceState(outState);
    }


    @Override
    public void onRecyclerViewCreated(RecyclerView list) {
        list.addItemDecoration(new PreferenceDividerDecoration(getContext()).drawBottom(true));
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference instanceof PreferenceScreen) {
            PreferenceScreen preferenceScreen = (PreferenceScreen) preference;
            mPreferenceScreenNavigation.onPreferenceScreenClick(preferenceScreen);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }


    @Override
    public void onNavigateToPreferenceScreen(PreferenceScreen preferenceScreen) {
        getActivity().setTitle(preferenceScreen.getTitle());
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param context current activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        uninstallReceiver = new UninstallReceiver();

        uninstallReceiver.setListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.OAT_UNINSTALL_BROADCAST);
        getActivity().registerReceiver(uninstallReceiver, intentFilter);
        super.onResume();
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(uninstallReceiver);
        super.onPause();
    }

    private Preference.OnPreferenceClickListener createOnPreferenceClickListener() {
        return new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                showToast(getString(R.string.uninstall_pending));
                try {
                    DeviceController.removeDevice(preference.getKey());
                } catch (UnsupportedEncodingException e) {
                    LOGGER.error(e);
                }


                return true;
            }

        };
    }

    @Override
    public void onUninstallSuccess(String prefKey) {
        switch (prefKey) {
            case Const.KEY_SYSTEM_APP_UNINSTALL:
                mListener.onSystemAppEnabled(false);
                break;
            case Const.KEY_OAT_UNINSTALL: {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:de.oat.ownantitheft"));
                startActivity(intent);
                break;
            }

        }
    }

    private void showToast(String message) {
        Toast.makeText(getContext(),
                message, Toast.LENGTH_LONG)
                .show();
    }

    /**
     * Listener for Activity interactions
     */
    public interface OnFragmentInteractionListener {
        void onSystemAppEnabled(boolean enabled);
    }
}
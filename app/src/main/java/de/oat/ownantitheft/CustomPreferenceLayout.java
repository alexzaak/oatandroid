package de.oat.ownantitheft;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;


/**
 * Custom Layout for preferences view witch supports a progressbar
 *
 * @author Alexander Zaak
 * 11.01.16.
 */
public class CustomPreferenceLayout extends Preference {

    private ProgressBar progressBar;

    /**
     * Constructor to set the customized Layout resource
     *
     * @param context application context
     * @param attrs   additional attributes
     */
    public CustomPreferenceLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setLayoutResource(R.layout.custom_config_layout);

    }

    /**
     *  Overridden Method to initialize the progressbar
     * @param holder holds all view elements of the preference view
     */
    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        progressBar = (ProgressBar) holder.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);

    }

    public void setIconVisibility(boolean visible) {
        getIcon().setVisible(visible, true);
    }

    /**
     * Method to change the visibility of the progressbar
     * @param visible true, change to visible, else false
     */
    public void setProgressVisibility(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}

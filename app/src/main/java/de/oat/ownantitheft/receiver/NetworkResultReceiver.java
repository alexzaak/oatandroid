package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import de.oat.ownantitheft.utils.Const;

/**
 * Class to receives results from RegisterGcmTask and ServerTask
 *
 * @author Alexander Zaak
 *         10.01.16.
 * @see de.oat.ownantitheft.network.RegisterGcmTask
 * @see de.oat.ownantitheft.network.ServerTask
 */
public class NetworkResultReceiver extends BroadcastReceiver {

    private OnNetworkTaskInteractionListener mListener;

    public void setListener(OnNetworkTaskInteractionListener mActivity) {
        this.mListener = mActivity;
    }

    /**
     * Called when result received from RegisterGcmTask or ServerTask
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        Bundle extra = intent.getExtras();

        if (action.equalsIgnoreCase(Const.GCM_TASK_BROADCAST)) {
            boolean isGcmRegistered = extra.getBoolean(Const.IS_GCM_REGISTERED, false);
            mListener.onGCMRegistered(isGcmRegistered);
        }

        if (action.equalsIgnoreCase(Const.DEVICE_TASK_BROADCAST)) {
            boolean isDeviceRegistered = extra.getBoolean(Const.IS_DEVICE_REGISTERED, false);
            mListener.onDeviceRegistered(isDeviceRegistered);
        }

        if (action.equalsIgnoreCase(Const.DEVICE_TASK_PROGRESS) || action.equalsIgnoreCase(Const.GCM_TASK_PROGRESS)) {
            mListener.onRegisterTaskProgress();
        }

        if (action.equalsIgnoreCase(Const.DATA_TASK_BROADCAST)) {
            mListener.onCredentialsSaved();
        }

        if (action.equalsIgnoreCase(Const.DATA_TASK_PROGRESS)) {

            mListener.onCredentialsProgressed();
        }
    }

    /**
     * Interface to interact between activity and the background tasks
     */
    public interface OnNetworkTaskInteractionListener {
        void onGCMRegistered(boolean registered);


        void onDeviceRegistered(boolean registered);

        void onRegisterTaskProgress();

        void onCredentialsSaved();

        void onCredentialsProgressed();
    }
}

package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.oat.ownantitheft.utils.Const;

/**
 * Class to receives broadcasts from encryption task
 *
 * @author Alexander Zaak
 *         10.01.16.
 * @see de.oat.ownantitheft.network.EncryptionTask
 */
public class EncryptionReceiver extends BroadcastReceiver {
    private OnEncryptionTaskListener mListener;

    /**
     * @param mActivity where Receiver is registered
     */
    public void setListener(OnEncryptionTaskListener mActivity) {
        this.mListener = mActivity;
    }

    /**
     * Called when encryption was finished
     *
     * @param context current context
     * @param intent  custom intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equalsIgnoreCase(Const.ENCRYPTION_FINISHED)) {
            mListener.onEncrypted();
        }
    }

    /**
     * Interface to interact between encryptionTask and activity
     */
    public interface OnEncryptionTaskListener {
        void onEncrypted();
    }
}

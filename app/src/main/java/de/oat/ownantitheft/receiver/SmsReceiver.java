package de.oat.ownantitheft.receiver;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.oat.ownantitheft.model.OATCommands;
import de.oat.ownantitheft.remote.OATSubCommander;
import de.oat.ownantitheft.utils.AndroidVersion;
import de.oat.ownantitheft.utils.Const;

/**
 * Class to receives sms and to controlDevice the app by sms
 *
 * @author Alexander Zaak
 *         20.08.15.
 */
public class SmsReceiver extends BroadcastReceiver {

    private static final Logger LOGGER = Logger.getLogger(SmsReceiver.class);

    /**
     * This method is called when a sms is receiving.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */

    @Override
    public void onReceive(Context context, Intent intent) {

        OATSubCommander commander = new OATSubCommander();
        Pattern smsPatter = Pattern.compile("^oat ([a-z0-9]{8}) (lock) ?([\\w\\.,@!?\\- äöüß]+)\\s*$|oat ([a-z0-9]*) (status|contacts|callback)\\s*$|^oat ([a-z0-9]*) ([a-z]*) ?([0-9]+)[\\s]*$", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);

        SmsMessage currentMessage = getSmsMessage(intent);

        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

        String smsBody = currentMessage.getDisplayMessageBody();

        Matcher matcher = smsPatter.matcher(smsBody);

        while (matcher.find()) {
                OATCommands OATCommands = new OATCommands(matcher, phoneNumber);

            commander.controlDevice(OATCommands, phoneNumber);
                markMessageRead(context, phoneNumber, smsBody);
        }

    }


    /**
     * Method to get the last SMS
     *
     * @param intent The Intent being received.
     * @return Sms Message
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private SmsMessage getSmsMessage(Intent intent) {
        if (AndroidVersion.isKitKat()) { //KITKAT

            SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            return msgs[0];

        } else {
            return getSmsMessageOlderDevices(intent);
        }
    }

    /**
     * Method to get last sms by older android versions
     *
     * @param intent The Intent being received.
     * @return sms message
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private SmsMessage getSmsMessageOlderDevices(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            LOGGER.error(Const.ERROR_BUNDLE_NULL);
            throw new IllegalArgumentException(Const.ERROR_BUNDLE_NULL);
        }

        Object[] pdusObj = (Object[]) bundle.get("pdus");

        if (pdusObj == null) {
            LOGGER.error(Const.ERROR_NOTNULL);
            throw new IllegalArgumentException(Const.ERROR_NOTNULL);
        }
        return SmsMessage.createFromPdu((byte[]) pdusObj[0]);
    }

    /**
     * Method to mark sms as read
     * Works only on android visions lower kitkat
     *
     * @param context The Context in which the receiver is running.
     * @param number  phone number of sms
     * @param body    body of the sms
     */
    private void markMessageRead(Context context, String number, String body) {

        Uri uri = Uri.parse("content://sms/inbox");
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        try {

            while (cursor.moveToNext()) {

                if ((cursor.getString(cursor.getColumnIndex("address")).equals(number)) && (cursor.getInt(cursor.getColumnIndex("read")) == 0)) {

                    if (cursor.getString(cursor.getColumnIndex("body")).startsWith(body)) {
                        String smsMessageId = cursor.getString(cursor.getColumnIndex("_id"));
                        ContentValues values = new ContentValues();
                        values.put("read", true);
                        context.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=" + smsMessageId, null);
                        return;
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }
}

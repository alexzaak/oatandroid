package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.oat.ownantitheft.SettingsActivity;
import de.oat.ownantitheft.utils.Const;

/**
 * Class to receives outgoing calls and to start hidden views
 *
 * @author Alexander Zaak
 *         10.08.15.
 */
public class DialReceiver extends BroadcastReceiver {

    /**
     * Called when an outgoing call has been sent
     *
     * @param context current context
     * @param intent  current system intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(android.content.Intent.ACTION_NEW_OUTGOING_CALL)) {
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

            Intent intent1 = new Intent(context, SettingsActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            switch (phoneNumber) {
                case Const.DIAL_NUMBER_MAIN_SCREEN:
                    intent1.putExtra(Const.FRAGMENT_TAG, Const.FRAGMENT_SETTINGS);
                    context.startActivity(intent1);
                    break;
                case Const.DIAL_NUMBER_UNINSTALL_SCREEN:
                    intent1.putExtra(Const.FRAGMENT_TAG, Const.FRAGMENT_UNINSTALL);
                    context.startActivity(intent1);
                    break;


            }

        }
    }
}

package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.R;
import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.device.Connectivity;
import de.oat.ownantitheft.utils.EmailSender;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class to detect an exchange of sim cards
 *
 * @author Alexander Zaak
 *         20.08.15.
 */
public class SimChangedReceiver extends BroadcastReceiver {

    private static final Logger LOGGER = Logger.getLogger(SimChangedReceiver.class);

    private PrefManager prefManager = new PrefManager();

    /**
     * This method is called when the BroadcastReceiver is receiving that system was completely booted
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        if (!PermissionsCheck.hasPermissionReadPhoneState()) {
            LOGGER.error("permission denied for phone state");
            return;
        }

        if (!Connectivity.hasTelephonyFeature()) {
            return;
        }


        // Checks Sim card State
        try {
            TelephonyManager telephoneMgr = AppController.getTelephonyManager();


            int simState = telephoneMgr.getSimState();

            switch (simState) {
                case TelephonyManager.SIM_STATE_UNKNOWN:
                    EmailSender.sendMessage(context.getString(R.string.subject_sim_change), context.getString(R.string.message_sim_removed));
                    break;
                case TelephonyManager.SIM_STATE_READY:
                    if (prefManager.getSimcardId() != null && !prefManager.getSimcardId().equals(telephoneMgr.getSubscriberId())) {
                        String message = String.format(context.getString(R.string.message_sim_change), telephoneMgr.getLine1Number());
                        EmailSender.sendMessage(context.getString(R.string.subject_sim_change), message);
                    }
                    break;
                default:
                    break;
            }


        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
    }


}

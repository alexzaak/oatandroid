package de.oat.ownantitheft.receiver;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.R;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.services.LockService;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class to receives changes by the device administrator
 * @author Alexander Zaak
 *         14.08.15.
 */
public class AdminReceiver extends DeviceAdminReceiver {

    private static final Logger LOGGER = Logger.getLogger(AdminReceiver.class);
    private PrefManager prefManager = new PrefManager();

    /**
     * @return A newly instantiated {@link android.content.ComponentName} for this
     * DeviceAdminReceiver.
     */
    public static ComponentName getComponentName(Context context) {
        return new ComponentName(context.getApplicationContext(), AdminReceiver.class);
    }


    /**
     * Called when app enabled as device admin
     *
     * @param context current context
     * @param intent  system intent
     */
    @Override
    public void onEnabled(Context context, Intent intent) {
        prefManager.setDeviceAdmin(true);
        sendStatus();
    }

    /**
     * Called when a disable request has been sent
     * @param context current context
     * @param intent system intent
     * @return warning message
     */
    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return context.getString(R.string.device_admin_disable_warning);
    }

    /**
     * Called when app disabled as device admin
     *
     * @param context current context
     * @param intent system intent
     */
    @Override
    public void onDisabled(Context context, Intent intent) {
        prefManager.setDeviceAdmin(false);
        sendStatus();
    }

    /**
     * Called when lockscreen password has been changed
     *
     * @param context current context
     * @param intent system intent
     */
    @Override
    public void onPasswordChanged(Context context, Intent intent) {

        showToast(context, context.getString(R.string.password_changed));
    }

    /**
     * Called when password input was incorrect
     *
     * @param context current context
     * @param intent system intent
     */
    @Override
    public void onPasswordFailed(Context context, Intent intent) {
        if (prefManager.isLocked()) {
            Intent serviceIntent = new Intent(context, LockService.class);
            serviceIntent.putExtra(Const.LOCK_ACTIVITY_COMMAND, Const.SHOW_LOCK_ACTIVITY);
            context.startService(serviceIntent);

            DeviceController.lockNow();
        }
    }

    /**
     * Called when password input was correct
     *
     * @param context current context
     * @param intent system intent
     */
    @Override
    public void onPasswordSucceeded(Context context, Intent intent) {
        prefManager.setLock(false);
        Intent lockService = new Intent(context, LockService.class);
        lockService.putExtra(Const.LOCK_ACTIVITY_COMMAND, Const.STOP_SERVICE);
        context.stopService(lockService);
    }

    /**
     *  Method to send status when app has been enabled/disabled as device admin
     */
    private void sendStatus() {
        try {
            DeviceController.getStatus(null);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Method to create a toast message
     *
     * @param context current context
     * @param msg     Message to show
     */
    private void showToast(Context context, CharSequence msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}

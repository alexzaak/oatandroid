package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.oat.ownantitheft.services.LockService;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PrefManager;

/**
 * Class to receives specific broadcast and to start the lock service
 * @author Alexander Zaak
 *         18.08.15.
 * @see de.oat.ownantitheft.services.LockService
 */
public class LockReceiver extends BroadcastReceiver {

    private PrefManager prefManager = new PrefManager();

    /**
     * Called when the boot progress of the device was completed or the screen goes off
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (prefManager.isLocked() && (action.equals(Intent.ACTION_BOOT_COMPLETED) || action.equals(Intent.ACTION_SCREEN_OFF))) {
            Intent lockService = new Intent(context, LockService.class);
            lockService.putExtra(Const.LOCK_ACTIVITY_COMMAND, Const.SHOW_LOCK_ACTIVITY);
            context.startService(lockService);

        }
    }
}


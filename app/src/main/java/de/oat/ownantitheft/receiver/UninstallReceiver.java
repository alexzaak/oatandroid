package de.oat.ownantitheft.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import de.oat.ownantitheft.utils.Const;

/**
 * Class to receives results from RegisterGcmTask and ServerTask
 *
 * @author Alexander Zaak
 *         10.01.16.
 * @see de.oat.ownantitheft.network.RegisterGcmTask
 * @see de.oat.ownantitheft.network.ServerTask
 */
public class UninstallReceiver extends BroadcastReceiver {
    private OnUninstallTaskInteractionListener mListener;

    public UninstallReceiver() {
    }

    public void setListener(OnUninstallTaskInteractionListener mActivity) {
        this.mListener = mActivity;
    }

    /**
     * Called when result received from RegisterGcmTask or ServerTask
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extra = intent.getExtras();

        if (action.equalsIgnoreCase(Const.OAT_UNINSTALL_BROADCAST)) {
            String prefKey = extra.getString(Const.PREF_KEY);
            mListener.onUninstallSuccess(prefKey);
        }
    }

    /**
     * Interface to interact between activity and the background tasks
     */
    public interface OnUninstallTaskInteractionListener {
        void onUninstallSuccess(String prefKey);

    }
}

package de.oat.ownantitheft.receiver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import de.oat.ownantitheft.services.GcmMessageService;

/**
 * Class to receives gcm messages
 *
 * @author Alexander Zaak
 *         17.08.15.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    /**
     * Called when gcm message has been received
     * Wake up the device
     *
     * @param context current context
     * @param intent  current system intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmMessageService.class.getName());

        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
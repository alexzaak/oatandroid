package de.oat.ownantitheft.device;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.provider.Settings;
import android.telephony.gsm.GsmCellLocation;

import org.apache.log4j.Logger;

import java.util.List;

import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.model.CellLocationInfo;
import de.oat.ownantitheft.model.Device;
import de.oat.ownantitheft.model.DeviceSettings;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.PrefManager;
import de.oat.ownantitheft.utils.SystemUtils;

/**
 * @author Alexander Zaak
 * 10.08.15
 */
public class DeviceStates {

    private static final Logger LOGGER = Logger.getLogger(DeviceStates.class);
    /**
     * Current application context
     */
    private static Context context = AppController.getInstance();
    /**
     * Preference-Manager to manage preferences quickly
     */
    private PrefManager prefManager = new PrefManager();

    /**
     * Method to get the imei of the device
     *
     * @return device imei, returns "0", if device hasn't a imei
     */
    private static String getImei() {
        if (!PermissionsCheck.hasPermissionReadPhoneState()) {
            LOGGER.error("Permission denied for phone state");
            return "0";
        }
        if (AppController.getTelephonyManager().getDeviceId() != null) {
            return AppController.getTelephonyManager().getDeviceId();
        } else {
            return "0";
        }
    }

    /**
     * Method to get the name of device, like "GT-I9505"
     *
     * @return device name as String
     */
    private static String getDeviceName() {
        return android.os.Build.MODEL;
    }

    /**
     * Method to get the current battery level of device
     *
     * @return current battery level in percent (0-100)
     */
    private static int getBatteryLevel() {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        if (batteryIntent == null) {
            return 0;
        }
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50;
        }
        return level;
    }

    /**
     * Method to get the Android-Id of the device.
     *
     * @return android-id
     */
    public static String getAndroidId() {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    /**
     * Method to toggle wifi.
     *
     * @param enable true to enable wifi, false to disable
     */
    public static void enableWifi(boolean enable) {
        AppController.getWifiManager().setWifiEnabled(enable);
    }

    /**
     * Method to toggle bluetooth.
     *
     * @param enable true to enable bluetooth, false to disable
     */
    public static void enableBt(boolean enable) {
        if (AppController.getBluetoothAdapter() != null) {
            if (enable) {
                AppController.getBluetoothAdapter().enable();
            } else {
                AppController.getBluetoothAdapter().disable();
            }
        }

    }

    /**
     * Method to verify whether bluetooth is enabled
     *
     * @return true if enabled, else false
     */
    private static boolean isBtEnabled() {
        if (AppController.getBluetoothAdapter() != null) {
            return AppController.getBluetoothAdapter().isEnabled();
        }
        return false;
    }

    /**
     * Method to get the current brightness setting
     *
     * @return current brightness (0-255)
     */
    private static int getBrightness() {
        try {
            return android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            LOGGER.error(e);
            return 0;
        }
    }

    /**
     * Method to set up the brightness of device.
     * Since Android 6.0 Marshmallow write settings permission is needed
     * @param brightness value from 0 to 255
     */
    public static void setBrightness(int brightness) {
        if (!PermissionsCheck.hasPermissionCanWrite()) {
            LOGGER.error("Permission denied for Write settings");
            return;
        }
        // Set systemwide brightness setting.
        Settings.System.putInt(context.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);


    }

    /**
     * Method to get current running processes
     */
    public static void getRunningProcesses() {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> recentTasks = activityManager.getRunningAppProcesses();

        for (int i = 0; i < recentTasks.size(); i++) {
            LOGGER.debug("Application executed : " + recentTasks.get(i).processName + "\t\t ID: ");
        }
    }

    /**
     * Method to verify whether wifi is enabled
     * @return true, if wifi is enabled, else false
     */
    private boolean isWifiEnabled() {
        return AppController.getWifiManager().isWifiEnabled();
    }

    /**
     * Method to verify whether gps-sensor is enabled
     *
     * @return true if gps is enabled, else false
     */
    private boolean isGpsEnabled() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Method to get all device data
     *
     * @return device data
     */
    public Device getDeviceStates() {

        Device device = new Device();

        device.setImei(getImei());
        device.setName(getDeviceName());
        device.setRegId(prefManager.getGcmRegId());
        device.setBatteryLevel(getBatteryLevel());

        device.setCellInfo(getCellInfo());
        device.setSettings(getDeviceSettings());

        return device;
    }

    /**
     * Method to get current device settings
     *
     * @return current device settings
     */
    private DeviceSettings getDeviceSettings() {
        DeviceSettings deviceSettings = new DeviceSettings();
        Connectivity connectivity = new Connectivity();

        deviceSettings.setWifi(isWifiEnabled());
        deviceSettings.setBrightness(getBrightness());
        deviceSettings.setBt(isBtEnabled());
        deviceSettings.setGps(isGpsEnabled());
        deviceSettings.setNetworkType(connectivity.getCellType());
        deviceSettings.setSync(Connectivity.isSync());
        deviceSettings.setCamera(DeviceController.isCameraEnabled());
        deviceSettings.setLocked(prefManager.isLocked());
        deviceSettings.setIsSystemApp(SystemUtils.isSystemApp());
        deviceSettings.setIsAdminEnabled(DeviceController.isAdminActive());
        return deviceSettings;
    }

    /**
     * Method to get current cell information
     * @return current cell information
     */
    private CellLocationInfo getCellInfo() {
        CellLocationInfo cellInfo = new CellLocationInfo();
        if (!PermissionsCheck.hasPermissionLocation()) {
            LOGGER.error("Permission denied for Location");
            return cellInfo;
        }
        GsmCellLocation location = (GsmCellLocation) AppController.getTelephonyManager().getCellLocation();
        String networkOperator = AppController.getTelephonyManager().getNetworkOperator();

        if (location != null && networkOperator != null && !networkOperator.isEmpty()) {
            int mcc = 0;
            int mnc = 0;
            try {
                mcc = Integer.parseInt(networkOperator.substring(0, 3));
                mnc = Integer.parseInt(networkOperator.substring(3));
            } catch (StringIndexOutOfBoundsException e) {
                LOGGER.error(e);
            }

            cellInfo.setLac(location.getLac());
            cellInfo.setCid(location.getCid());
            cellInfo.setMcc(mcc);
            cellInfo.setMnc(mnc);

        }

        return cellInfo;
    }


}

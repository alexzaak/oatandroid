package de.oat.ownantitheft.device;

import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import org.apache.log4j.Logger;

import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.utils.Const;

/**
 * @author Alexander Zaak
 * 18.08.15.
 */
public class Connectivity {
    private static final Logger LOGGER = Logger.getLogger(Connectivity.class);

    /**
     * Method to verify whether the device has telephony feature.
     *
     * @return true, if device has telephony feature, else false
     */
    public static boolean hasTelephonyFeature() {
        return AppController.getMainPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    /**
     * Method to verify whether sync is enabled.
     *
     * @return true, if sync is enabled, else false
     */
    public static boolean isSync() {
        return ContentResolver.getMasterSyncAutomatically();
    }

    /**
     * Method to get the current connection type
     *
     * @return current connection type, if not connected return value -1
     */
    public int getCellType() {

        NetworkInfo info = AppController.getConnectivityManager().getActiveNetworkInfo();

        if (info == null || !info.isConnected()) {
            return Const.CELL_TYPE_NONE; //not connected
        }
        if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            return Const.CELL_TYPE_WIFI;
        }
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return Const.CELL_TYPE_GSM;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                    return Const.CELL_TYPE_CDMA;
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                    return Const.CELL_TYPE_LTE;
                default:
                    return Const.CELL_TYPE_NONE;
            }
        }
        return Const.CELL_TYPE_NONE;
    }
}

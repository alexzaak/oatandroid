package de.oat.ownantitheft.configs;

import android.content.Context;
import android.os.Environment;

import org.apache.log4j.Level;

import java.io.File;

import de.mindpipe.android.logging.log4j.LogConfigurator;
import de.oat.ownantitheft.utils.PermissionsCheck;

/**
 * Created by Alexander Zaak on 13.08.15.
 */
public class ConfigureLog4J {
    private ConfigureLog4J() {

    }

    public static void configure(Context context) throws Exception {
        final LogConfigurator logConfigurator = new LogConfigurator();

        if (PermissionsCheck.hasPermissionWriteStorage()) {
            if (Environment.isExternalStorageEmulated()) {
                logConfigurator.setFileName(Environment.getExternalStorageDirectory().toString() + File.separator + "oat/log/oat_log.txt");
            } else {
                logConfigurator.setFileName(context.getFilesDir().toString() + File.separator + "oat/log/oat_log.txt");
            }
        }

        logConfigurator.setRootLevel(Level.ALL);
        logConfigurator.setLevel("org.apache", Level.ALL);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxBackupSize(10);
        logConfigurator.setMaxFileSize(1024 * 512);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
    }
}

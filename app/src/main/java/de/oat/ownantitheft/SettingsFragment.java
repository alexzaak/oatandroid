package de.oat.ownantitheft;


import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.XpPreferenceFragment;
import android.support.v7.widget.PreferenceDividerDecoration;
import android.support.v7.widget.RecyclerView;

import net.xpece.android.support.preference.PreferenceScreenNavigationStrategy;
import net.xpece.android.support.preference.SwitchPreference;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.receiver.NetworkResultReceiver;
import de.oat.ownantitheft.remote.DeviceController;
import de.oat.ownantitheft.utils.AndroidVersion;
import de.oat.ownantitheft.utils.Const;
import de.oat.ownantitheft.utils.PermissionsCheck;
import de.oat.ownantitheft.utils.SystemUtils;

/**
 * @author Alexander Zaak
 *         08.12.2015
 */
public class SettingsFragment extends XpPreferenceFragment implements
        PreferenceScreenNavigationStrategy.Callbacks, NetworkResultReceiver.OnNetworkTaskInteractionListener {


    private static final Logger LOGGER = Logger.getLogger(SettingsFragment.class);
    private OnFragmentInteractionListener mListener;

    private SwitchPreference setDeviceAdmin;
    private CustomPreferenceLayout oatConfig;

    private SwitchPreference switchAllPermissions;
    private SwitchPreference switchContactPermission;
    private SwitchPreference switchSmsPermission;
    private SwitchPreference switchStoragePermission;
    private SwitchPreference switchLocationPermission;
    private SwitchPreference switchTelephonyPermission;

    // These are used to navigate back and forth between subscreens.
    private PreferenceScreenNavigationStrategy mPreferenceScreenNavigation;

    private NetworkResultReceiver networkResultReceiver;


    @Override
    public void onCreatePreferences2(final Bundle savedInstanceState, final String rootKey) {
        // Add 'general' preferences.

        addPreferencesFromResource(R.xml.pref_configuration);

        // Add 'permission' preferences, and a corresponding header.
        PreferenceCategory prefHeader;
        if (AndroidVersion.isMarshmallow()) {
            prefHeader = new PreferenceCategory(getPreferenceManager().getContext());
            prefHeader.setTitle(R.string.permission);
            getPreferenceScreen().addPreference(prefHeader);


            addPreferencesFromResource(R.xml.pref_permission);

            switchAllPermissions = (SwitchPreference) findPreference(Const.KEY_PERMISSION_ALL);
            switchContactPermission = (SwitchPreference) findPreference(Const.KEY_PERMISSION_CONTACT);
            switchSmsPermission = (SwitchPreference) findPreference(Const.KEY_PERMISSION_SMS);
            switchStoragePermission = (SwitchPreference) findPreference(Const.KEY_PERMISSION_STORAGE);
            switchLocationPermission = (SwitchPreference) findPreference(Const.KEY_PERMISSION_LOCATION);
            switchTelephonyPermission = (SwitchPreference) findPreference(Const.KEY_PERMISSION_TELEPHONY);

            isPermissionEnabled();

            switchAllPermissions.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
            switchContactPermission.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
            switchSmsPermission.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
            switchStoragePermission.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
            switchLocationPermission.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
            switchTelephonyPermission.setOnPreferenceChangeListener(createOnPreferenceChangeListener());
        }
        prefHeader = new PreferenceCategory(getPreferenceManager().getContext());
        prefHeader.setTitle(R.string.advanced);
        getPreferenceScreen().addPreference(prefHeader);
        addPreferencesFromResource(R.xml.pref_advanced);


        oatConfig = (CustomPreferenceLayout) findPreference(Const.KEY_OAT_CONFIG);

        setOatConfigStatus(DeviceController.isInitSuccess());
        setDeviceAdmin = (SwitchPreference) findPreference(Const.KEY_DEVICE_ADMIN);
        SwitchPreference setSystemApp = (SwitchPreference) findPreference(Const.KEY_SYSTEM_APP);

        boolean adminEnabled = DeviceController.isAdminActive();
        setDeviceAdminCheckbox(adminEnabled);
        setDeviceAdmin.setChecked(adminEnabled);
        setSystemApp.setChecked(SystemUtils.isSystemApp());
        setSystemApp.setEnabled(!SystemUtils.isSystemApp());

        setSystemApp.setVisible(SystemUtils.isRooted());
        setDeviceAdmin.setOnPreferenceChangeListener(createOnPreferenceChangeListener());


        setSystemApp.setOnPreferenceChangeListener(createOnPreferenceChangeListener());

        mPreferenceScreenNavigation = new PreferenceScreenNavigationStrategy.ReplaceRoot(this, this);
        mPreferenceScreenNavigation.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        networkResultReceiver = new NetworkResultReceiver();

        networkResultReceiver.setListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.DATA_TASK_PROGRESS);
        intentFilter.addAction(Const.DEVICE_TASK_BROADCAST);
        intentFilter.addAction(Const.GCM_TASK_BROADCAST);
        intentFilter.addAction(Const.DEVICE_TASK_PROGRESS);
        intentFilter.addAction(Const.GCM_TASK_PROGRESS);
        intentFilter.addAction(Const.DATA_TASK_BROADCAST);


        getActivity().registerReceiver(networkResultReceiver, intentFilter);
        super.onResume();
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(networkResultReceiver);
        super.onPause();
    }

    @Override
    public void onGCMRegistered(boolean registered) {
        if (registered) {
            try {
                DeviceController.getStatus();
            } catch (UnsupportedEncodingException e) {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void onDeviceRegistered(boolean registered) {
        if (registered) {
            DeviceController.storeSimcardID();
            DeviceController.initSuccess();
        }
        setProgressVisibility(!registered);
        setOatConfigStatus(registered);


    }

    @Override
    public void onRegisterTaskProgress() {
        setProgressVisibility(true);

    }

    @Override
    public void onCredentialsSaved() {
        DeviceController.registerGcm();
    }

    @Override
    public void onCredentialsProgressed() {
        setProgressVisibility(true);

    }

    public void setOatConfigStatus(boolean success) {
        if (success) {
            oatConfig.setIcon(R.mipmap.ic_success);

        } else {
            oatConfig.setIcon(R.mipmap.ic_error);
        }
    }

    public void setDeviceAdminCheckbox(boolean enabled) {
        setDeviceAdmin.setChecked(enabled);
    }

    public void isPermissionEnabled() {
        boolean hasAllPermissions = PermissionsCheck.hasAllPermissions();
        boolean hasPermissionReadContacts = PermissionsCheck.hasPermissionReadContacts();
        boolean hasPermissionSMS = PermissionsCheck.hasPermissionSMS();
        boolean hasPermissionWriteStorage = PermissionsCheck.hasPermissionWriteStorage();
        boolean hasPermissionLocation = PermissionsCheck.hasPermissionLocation();
        boolean hasPermissionTelephony = PermissionsCheck.hasPermissionTelephony();

        switchAllPermissions.setChecked(hasAllPermissions);
        switchAllPermissions.setEnabled(!hasAllPermissions);

        switchContactPermission.setChecked(hasPermissionReadContacts);
        switchContactPermission.setEnabled(!hasPermissionReadContacts);

        switchSmsPermission.setChecked(hasPermissionSMS);
        switchSmsPermission.setEnabled(!hasPermissionSMS);

        switchStoragePermission.setChecked(hasPermissionWriteStorage);
        switchStoragePermission.setEnabled(!hasPermissionWriteStorage);

        switchLocationPermission.setChecked(hasPermissionLocation);
        switchLocationPermission.setEnabled(!hasPermissionLocation);

        switchTelephonyPermission.setChecked(hasPermissionTelephony);
        switchTelephonyPermission.setEnabled(!hasPermissionTelephony);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mPreferenceScreenNavigation.onSaveInstanceState(outState);
    }

    @Override
    public void onRecyclerViewCreated(RecyclerView list) {
        list.addItemDecoration(new PreferenceDividerDecoration(getContext()).drawBottom(true));
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference instanceof PreferenceScreen) {
            PreferenceScreen preferenceScreen = (PreferenceScreen) preference;
            mPreferenceScreenNavigation.onPreferenceScreenClick(preferenceScreen);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param context current activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onNavigateToPreferenceScreen(PreferenceScreen preferenceScreen) {
        getActivity().setTitle(preferenceScreen.getTitle());
    }

    private Preference.OnPreferenceChangeListener createOnPreferenceChangeListener() {
        return new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                boolean enabled = (boolean) newValue;
                switch (preference.getKey()) {
                    case Const.KEY_DEVICE_ADMIN:
                        mListener.onDeviceAdminEnabled(enabled, Const.REQUEST_CODE_DEVICE_ADMIN_SETTINGS_FRG);
                        break;
                    case Const.KEY_SYSTEM_APP:
                        preference.setEnabled(!enabled);
                        mListener.onSystemAppEnabled(enabled);
                        break;
                    case Const.KEY_PERMISSION_ALL:
                        mListener.onAllPermissionsEnabled(enabled);
                        preference.setEnabled(!enabled);
                        break;
                    case Const.KEY_PERMISSION_CONTACT:
                        mListener.onContactPermissionEnabled(enabled);
                        break;
                    case Const.KEY_PERMISSION_SMS:
                        mListener.onSmsPermissionEnabled(enabled);
                        preference.setEnabled(!enabled);
                        break;
                    case Const.KEY_PERMISSION_STORAGE:
                        mListener.onStoragePermissionEnabled(enabled);
                        preference.setEnabled(!enabled);
                        break;
                    case Const.KEY_PERMISSION_LOCATION:
                        mListener.onLocationEnabled(enabled);
                        preference.setEnabled(!enabled);
                        break;
                    case Const.KEY_PERMISSION_TELEPHONY:
                        mListener.onTelephonyEnabled(enabled);
                        preference.setEnabled(!enabled);
                        break;
                }


                LOGGER.debug(preference.getKey() + " " + newValue.toString());

                return true;
            }
        };
    }

    public void setProgressVisibility(boolean progressVisibility) {
        this.oatConfig.getIcon().setVisible(!progressVisibility, false);
        this.oatConfig.setProgressVisibility(progressVisibility);
    }

    /**
     * Listener for Activity interactions
     */
    public interface OnFragmentInteractionListener {
        void onDeviceAdminEnabled(boolean enabled, int requestCode);

        void onSystemAppEnabled(boolean enabled);

        void onAllPermissionsEnabled(boolean enabled);

        void onContactPermissionEnabled(boolean enabled);

        void onSmsPermissionEnabled(boolean enabled);

        void onStoragePermissionEnabled(boolean enabled);

        void onLocationEnabled(boolean enabled);

        void onTelephonyEnabled(boolean enabled);
    }
}
package de.oat.ownantitheft.utils;

import android.Manifest;

/**
 * Created by Alexander Zaak on 10.08.15.
 */
public class Const {

    public static final String DIAL_NUMBER_MAIN_SCREEN = "*#3101091017030601#";
    public static final String DIAL_NUMBER_UNINSTALL_SCREEN = "*#31010910170306012211#";

    public static final int REQUEST_CODE_DEVICE_ADMIN_SETTINGS_FRG = 101;
    public static final int PERMISSIONS_REQUEST_CODE = 103;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 104;
    public static final int CELL_TYPE_GSM = 1;
    public static final int CELL_TYPE_CDMA = 2;
    public static final int CELL_TYPE_LTE = 3;
    public static final int CELL_TYPE_NONE = -1;
    public static final int CELL_TYPE_WIFI = 4;

    public static final String HOST_ADDRESS = "host_address";

    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";


    public static final String ERROR_NOTNULL = "Error urlConnection class should not be null";
    public static final String ERROR_BACKGROUND_TASK = "Error while processing in background ";
    public static final String API_ROUTE = "/v1/devices/";

    public static final String GCM_REG_ID = "gcm_reg_id";
    public static final String GCM_VALUE = "value";
    public static final String GCM_ENABLED = "enabled";
    public static final String GCM_ALL = "all";
    public static final String GCM_LEVEL = "level";
    public static final String GCM_CNO = "cno";
    public static final String GCM_MSG = "message";
    public static final String GCM_DURATION = "duration";

    public static final String GCM_SENDER_ID = "sender_id";
    public static final String CONF_PARAM = "conf";
    public static final String OWNER_INFORMATION = "owner_information";
    public static final String OWNER_PHONE_NUMBER = "owner_phone_number";
    public static final String GCM_PHONE_NO = "phoneno";
    public static final String IS_DEVICE_LOCKED = "is_device_locked";
    public static final String ALARM_DURATION = "alarm_duration";
    public static final String API_LOCATION_ROUTE = "/location";
    public static final String API_REMOVE_ROUTE = "/remove";
    public static final String SIM_SUBSCRIBER_ID = "sim_subscriber_id";
    public static final String API_MESSAGE_ROUTE = "/message-to-owner";
    public static final String API_CONTACTS_ROUTE = "/contacts-to-owner";
    public static final String GPS_DURATION = "gps_duration";


    public static final String GPS_ENABLE = "gps_enable";
    public static final String GPS_PHONENUMBER = "phonenumber";


    public static final String ERROR_INTENT_NULL = "intent is null";
    public static final String ERROR_BUNDLE_NULL = "bundle is null";
    public static final String ERROR_UNKNOWN_INDEX = "unknown index";

    public static final String IS_DEVICE_ADMIN_ENABLED = "is_device_admin_enabled";
    public static final String LOCK_ACTIVITY_COMMAND = "lock_activity_command";
    public static final int HIDE_LOCK_ACTIVITY = 601;
    public static final int SHOW_LOCK_ACTIVITY = 3101;
    public static final int CANCEL_LOCK_COUNTDOWN = 910;
    public static final int STOP_SERVICE = 1703;
    public static final String PREFS_INITIALIZED = "prefs_initialized";

    public static final String KEY_PERMISSION_ALL = "permission_all";
    public static final String KEY_PERMISSION_CONTACT = "permission_contact";
    public static final String KEY_PERMISSION_SMS = "permission_sms";
    public static final String KEY_PERMISSION_STORAGE = "permission_storage";
    public static final String KEY_PERMISSION_LOCATION = "permission_location";
    public static final String KEY_PERMISSION_TELEPHONY = "permission_telephony";
    public static final String KEY_DEVICE_ADMIN = "is_device_admin";
    public static final String KEY_SYSTEM_APP = "is_system_app";

    public static final String[] REQUIRED_PERMISSIONS = {Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    public static final String[] CONTACT_PERMISSIONS = {
            Manifest.permission.READ_CONTACTS,
    };

    public static final String[] STORAGE_PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final String[] TELEPHONY_PERMISSIONS = {Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
    };

    public static final String[] SMS_PERMISSIONS = {
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS,
    };

    public static final String[] LOCATION_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    public static final String KEY_OAT_CONFIG = "oat_config";
    public static final String KEY_OAT_UNINSTALL = "oat_uninstall";
    public static final String FRAGMENT_SETTINGS = "fragment_settings";
    public static final String FRAGMENT_UNINSTALL = "fragemnt_uninstall";

    public static final String GCM_TASK_BROADCAST = "gcm_task_broadcast";
    public static final String IS_GCM_REGISTERED = "is_gcm_registered";
    public static final String DEVICE_TASK_BROADCAST = "device_task_broadcast";
    public static final String IS_DEVICE_REGISTERED = "is_device_registered";
    public static final String SYSTEM_APP_TASK_BROADCAST = "system_app_task_broadcast";
    public static final String IS_SYSTEM_APP_REGISTERED = "is_system_app_registered";

    public static final String METHOD_POST = "POST";
    public static final String METHOD_DELETE = "DELETE";
    public static final String ENCRYPTION = "encryption";
    public static final String ENCRYPTION_FINISHED = "encryption_finished";
    public static final String KEY_SYSTEM_APP_UNINSTALL = "uninstall_system_app";

    public static final String FRAGMENT_TAG = "fragment_tag";
    public static final String DEVICE_TASK_PROGRESS = "device_task_progress";
    public static final String GCM_TASK_PROGRESS = "gcm_task_progress";
    public static final String DATA_TASK_BROADCAST = "data_task_broadcast";
    public static final String DATA_TASK_PROGRESS = "data_task_progress_deaefseda";
    public static final String OAT_UNINSTALL_BROADCAST = "oat_uninstall_broadcast";
    public static final String PREF_KEY = "pref_key";

    private Const() {

    }
}

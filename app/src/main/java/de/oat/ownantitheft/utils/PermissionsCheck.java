package de.oat.ownantitheft.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import de.oat.ownantitheft.controller.AppController;

/**
 * Created by Alexander Zaak on 01.12.15.
 */
public class PermissionsCheck {

    private static Context context = AppController.getInstance();


    @TargetApi(Build.VERSION_CODES.M)
    public static String[] checkRequiredPermissions(String[] requiredPermissions) {
        List<String> permissionsToRequest = new ArrayList<>();

        for (String permission : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission);

            }
        }


        return permissionsToRequest.toArray(new String[permissionsToRequest.size()]);

    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void requestPermissions(Activity activity, String[] permissions) {
        if (AndroidVersion.isMarshmallow() && permissions.length > 0) {
            activity.requestPermissions(permissions, Const.PERMISSIONS_REQUEST_CODE);

        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionReadPhoneState() {
        return !AndroidVersion.isMarshmallow() || ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionCallPhone() {
        if (AndroidVersion.isMarshmallow()) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionReadSMS() {
        if (AndroidVersion.isMarshmallow()) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionSendSMS() {
        if (AndroidVersion.isMarshmallow()) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionReadContacts() {
        if (AndroidVersion.isMarshmallow()) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionWriteStorage() {
        if (AndroidVersion.isMarshmallow()) {
            return ContextCompat.checkSelfPermission(context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionLocation() {
        if (AndroidVersion.isMarshmallow()) {
            int fineLocation = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            int coarseLocation = ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION);

            return fineLocation == PackageManager.PERMISSION_GRANTED && coarseLocation == PackageManager.PERMISSION_GRANTED;


        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionDrawOverlays() {
        if (AndroidVersion.isMarshmallow()) {
            return Settings.canDrawOverlays(context);
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermissionCanWrite() {
        if (AndroidVersion.isMarshmallow()) {
            return Settings.System.canWrite(context);
        }
        return true;
    }

    public static boolean hasPermissionSMS() {
        return hasPermissionSendSMS() && hasPermissionReadSMS();
    }

    public static boolean hasPermissionTelephony() {
        return hasPermissionCallPhone() && hasPermissionReadPhoneState();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasAllPermissions() {
        boolean hasAllPermissions = true;

        for (String permission : Const.REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                hasAllPermissions = false;

            }
        }
        return hasAllPermissions;

    }
}

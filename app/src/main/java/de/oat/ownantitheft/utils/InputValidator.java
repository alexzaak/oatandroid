package de.oat.ownantitheft.utils;

import android.content.Context;
import android.widget.EditText;

import de.oat.ownantitheft.R;

/**
 * Created by Alexander Zaak on 24.10.15.
 */
public class InputValidator {

    private InputValidator() {

    }

    public static boolean notEmpty(Context context, EditText editText) {
        if (editText.getText().toString().length() == 0) {
            editText.setError(context.getString(R.string.not_empty));
            return false;
        }
        return true;
    }
}

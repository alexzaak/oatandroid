package de.oat.ownantitheft.utils;

/**
 * Created by Alexander Zaak on 13.08.15.
 */
public class Commands {

    public static final int STATUS = 0;
    public static final int LOCK = 1;
    public static final int ALARM = 2;
    public static final int RESET_PASSWORD = 3;
    public static final int ENCRYPT_STORAGE = 4;
    public static final int SET_SECURE_SETTINGS = 5;
    public static final int SET_GLOBAL_SETTINGS = 6;
    public static final int DISABLE_CAMERA = 7;
    public static final int ENABLE_MASTER_SYNC = 8;
    public static final int WIPE = 9;
    public static final int ENABLE_WIFI = 10;
    public static final int ENABLE_BT = 11;
    public static final int ENABLE_GPS = 12;
    public static final int SET_BRIGHTNESS = 13;
    public static final int LOCALE_WITH_GPS = 14;
    public static final int GET_CONTACTS = 15;
    public static final int CALL_ME = 16;


    public static final String SMS_CMD_LOCK = "lock";
    public static final String SMS_CMD_STATUS = "status";
    public static final String SMS_CMD_ALARM = "alarm";
    public static final String SMS_CMD_ENCRYPT = "encrypt";
    public static final String SMS_CMD_CAMERA = "camera";
    public static final String SMS_CMD_SYNC = "sync";
    public static final String SMS_CMD_WIPE = "wipe";
    public static final String SMS_CMD_WIFI = "wifi";
    public static final String SMS_CMD_BT = "bt";
    public static final String SMS_CMD_GPS = "gps";
    public static final String SMS_CMD_BRIGHT = "bright";
    public static final String SMS_CMD_LOCATE = "locate";
    public static final String SMS_CMD_CONTACTS = "contacts";


    public static final String SMS_CMD_SECURE = "secure";
    public static final String SMS_CMD_CALLBACK = "callback";

    private Commands() {

    }

}

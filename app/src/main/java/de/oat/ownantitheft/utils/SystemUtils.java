package de.oat.ownantitheft.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.stericson.RootShell.exceptions.RootDeniedException;
import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import de.oat.ownantitheft.R;
import de.oat.ownantitheft.controller.AppController;

/**
 * Created by Alexander Zaak on 11.09.15.
 */
public class SystemUtils {
    private static final Logger LOGGER = Logger.getLogger(SystemUtils.class);

    private static String appName;

    private static String PATH_TO_SYSTEM_OLDER_ANDROID = "/system/app/";
    private static Context context = AppController.getInstance();

    private SystemUtils() {

    }

    public static boolean setAsSystemApp(boolean enabled) {

        appName = context.getString(R.string.app_name);


        try {
            ApplicationInfo localApplicationInfo = AppController.getAppInfo();

            if (enabled) {
                if (AndroidVersion.isKitKat()) {

                    toSystemAppKitkat(localApplicationInfo);
                } else {
                    toSystemAppICS(localApplicationInfo);

                }
            } else {
                removeSystemApp();
            }

        } catch (PackageManager.NameNotFoundException e) {
            LOGGER.error(e);
            return false;
        } catch (TimeoutException e) {
            LOGGER.error(e);
            return false;
        } catch (RootDeniedException e) {
            LOGGER.error(e);
            return false;
        } catch (IOException e) {
            LOGGER.error(e);
            return false;
        }
        return true;

    }

    private static void toSystemAppKitkat(ApplicationInfo localApplicationInfo) throws TimeoutException, RootDeniedException, IOException {

        String systemPathKitKat = "/system/priv-app/";
        String systemPath = systemPathKitKat + appName;
        String sourcePath = localApplicationInfo.sourceDir;

        String makeDirCommand = String.format("busybox mkdir %s", systemPath);

        String copyCommand = String.format("busybox cp %s %s", sourcePath, systemPath);

        String chmodCommand = String.format("%s %s", "busybox chmod -R 755", systemPath);

        String removeCommand = String.format("%s %s", "busybox rm -rf", sourcePath.substring(0, sourcePath.lastIndexOf("/")));

        setSystemAccess(true);


        Command commands = new Command(0, makeDirCommand, copyCommand, chmodCommand, removeCommand);


        RootTools.getShell(true).add(commands);


        setSystemAccess(false);

        Command command2 = new Command(0, "reboot");

        RootTools.getShell(true).add(command2);

    }


    private static void toSystemAppICS(ApplicationInfo applicationInfo) throws TimeoutException, RootDeniedException, IOException {

        String fileExtension = ".apk";
        String pathToSystem = PATH_TO_SYSTEM_OLDER_ANDROID + appName + fileExtension;

        String moveCommand = String.format("%s %s %s", "busybox mv", applicationInfo.sourceDir, pathToSystem);


        String chmodCommand = String.format("%s %s", "busybox chmod 644", pathToSystem);


        setSystemAccess(true);


        Command commands = new Command(0, moveCommand, chmodCommand);


        RootTools.getShell(true).add(commands);


        setSystemAccess(false);

        Command command2 = new Command(0, "reboot");

        RootTools.getShell(true).add(command2);
    }

    public static boolean isSystemApp() {
        try {
            ApplicationInfo appInfo = AppController.getAppInfo();

            return (appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            LOGGER.error(e);
            return false;
        }
    }


    public static boolean isRooted() {
        return RootTools.isRootAvailable();
    }

    private static void setSystemAccess(boolean enabled) {
        if (enabled) {
            RootTools.remount("/system", "rw");
            RootTools.remount("/mnt", "rw");
        } else {
            RootTools.remount("/system", "ro");
            RootTools.remount("/mnt", "ro");
        }
    }


    private static void removeSystemApp() throws TimeoutException, RootDeniedException, IOException {


        String systemPath;

        if (AndroidVersion.isOlderKitKat()) {
            String fileExtension = ".apk";
            systemPath = PATH_TO_SYSTEM_OLDER_ANDROID + appName + fileExtension;
        } else {
            String systemPathKitKat = "/system/priv-app/";
            systemPath = systemPathKitKat + appName;
        }


        String chmodCommand = String.format("chmod 777 %s", systemPath);

        String removeCommand = String.format("rm -rf %s", systemPath);

        setSystemAccess(true);


        Command commands = new Command(0, chmodCommand, removeCommand);


        RootTools.getShell(true).add(commands);


        setSystemAccess(false);

        Command command2 = new Command(0, "reboot");

        RootTools.getShell(true).add(command2);

    }

}

package de.oat.ownantitheft.utils;

import java.io.UnsupportedEncodingException;

import de.oat.ownantitheft.model.UserNotify;
import de.oat.ownantitheft.remote.DeviceController;

/**
 * Created by Alexander Zaak on 24.10.15.
 */
public class EmailSender {

    private EmailSender() {

    }

    public static void sendMessage(String subject, String message) throws UnsupportedEncodingException {
        UserNotify userNotify = new UserNotify();
        userNotify.setSubject(subject);
        userNotify.setMessage(message);
        DeviceController.sendMessage(userNotify);
    }
}

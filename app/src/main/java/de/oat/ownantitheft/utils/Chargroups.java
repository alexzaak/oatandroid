package de.oat.ownantitheft.utils;

/**
 * Created by Alexander Zaak on 11.09.15.
 */
public enum Chargroups {

    DIGITS("0123456789", 0), LOWER("abcdefghijklmnopqrstuvwxyz", 1);


    private final String chars;

    private final int index;


    Chargroups(String chars, int index) {
        this.chars = chars;
        this.index = index;
    }

    /**
     * returns count of chargroup
     *
     * @return
     */

    public static int getGroupCount() {
        return 2;
    }

    /**
     * returns chargoup for index
     *
     * @param index
     * @return
     */
    public static Chargroups getFromIndex(int index) {

        switch (index) {
            case 0:
                return DIGITS;
            case 1:
                return LOWER;
            default:
                throw new IllegalArgumentException(Const.ERROR_UNKNOWN_INDEX);

        }

    }

    /**
     * returns chars of Groups as Strings
     *
     * @return
     */

    public String getChars() {
        return chars;
    }

    /**
     * returns index of chargroup
     *
     * @return
     */

    public int getIndex() {
        return index;
    }
}

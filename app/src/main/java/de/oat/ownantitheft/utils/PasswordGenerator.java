package de.oat.ownantitheft.utils;

import java.util.Random;

/**
 * Created by Alexander Zaak on 11.09.15.
 */
public class PasswordGenerator {

    private static final int PASSWD_LENGTH = 6;

    private static final Random RAND = new Random();


    private PasswordGenerator() {

    }


    /**
     * generate password
     *
     * @return random password
     */
    public static String generatePassword() {
        StringBuilder builder = new StringBuilder(PASSWD_LENGTH);

        for (int i = 0; i < PASSWD_LENGTH; i++) {
            Chargroups group = Chargroups.getFromIndex(RAND.nextInt(Chargroups.getGroupCount()));
            char c = group.getChars().charAt(RAND.nextInt(group.getChars().length()));
            builder.append(c);
        }

        return builder.toString();

    }
}

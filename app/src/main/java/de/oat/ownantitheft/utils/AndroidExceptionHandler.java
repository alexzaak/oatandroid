package de.oat.ownantitheft.utils;

import org.apache.log4j.Logger;

/**
 * Created by Alexander Zaak on 24.09.15.
 */
public class AndroidExceptionHandler implements Thread.UncaughtExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(AndroidExceptionHandler.class);
    private Thread.UncaughtExceptionHandler androidDefaultUEH;

    /**
     * The thread is being terminated by an uncaught exception. Further
     * exceptions thrown in this method are prevent the remainder of the
     * method from executing, but are otherwise ignored.
     *
     * @param thread the thread that has an uncaught exception
     * @param ex     the exception that was thrown
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        LOGGER.error(ex.getMessage());
        LOGGER.error(ex.getLocalizedMessage());
        LOGGER.error(ex.getCause());

        androidDefaultUEH.uncaughtException(thread, ex);
    }
}

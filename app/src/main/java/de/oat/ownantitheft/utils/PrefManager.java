package de.oat.ownantitheft.utils;

import android.content.Context;
import android.content.SharedPreferences;

import de.oat.ownantitheft.controller.AppController;
import de.oat.ownantitheft.device.DeviceStates;

/**
 * Created by Alexander Zaak on 10.08.15.
 */
public class PrefManager {
    // Shared pref file name
    private static final String PREF_NAME = "OwnAntiTheft.xml";
    // Shared Preferences
    //private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    private SharedPreferences pref;

    // Constructor
    public PrefManager() {
        Context context = AppController.getInstance();
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        //pref = new SecurePreferences(context, DeviceStates.getAndroidId(), PREF_NAME);
        editor = pref.edit();
    }

    public boolean prefsExists() {
        return pref.contains(Const.PREFS_INITIALIZED);
    }


    public void storeHostAddress(String host) {

        // Storing host in pref
        editor.putString(Const.HOST_ADDRESS, host);

        // commit changes
        editor.commit();
    }


    public void storeGcmRegId(String token) {
        editor.putString(Const.GCM_REG_ID, token);
        editor.commit();
    }

    public String getGcmRegId() {
        return pref.getString(Const.GCM_REG_ID, null);
    }

    public void storeEmail(String email) {
        editor.putString(Const.CLIENT_ID, email);
        editor.commit();
    }

    public void storePassword(String pass) {
        editor.putString(Const.CLIENT_SECRET, pass);
        editor.commit();
    }

    public void storeSenderId(String senderId) {
        editor.putString(Const.GCM_SENDER_ID, senderId);
        editor.commit();
    }

    public String getEmail() {
        return pref.getString(Const.CLIENT_ID, null);
    }

    public String getPassword() {
        return pref.getString(Const.CLIENT_SECRET, null);
    }

    public String getHostAddress() {
        return pref.getString(Const.HOST_ADDRESS, null);
    }

    public String getSenderId() {
        return pref.getString(Const.GCM_SENDER_ID, null);
    }

    public String getApiAddress() {

        return getHostAddress() + Const.API_ROUTE + DeviceStates.getAndroidId();
    }

    public String getLocationApiAddress() {

        return getHostAddress() + Const.API_ROUTE + DeviceStates.getAndroidId() + Const.API_LOCATION_ROUTE;
    }

    public String getMessageApiAddress() {

        return getHostAddress() + Const.API_ROUTE + DeviceStates.getAndroidId() + Const.API_MESSAGE_ROUTE;
    }

    public String getContactsApiAddress() {

        return getHostAddress() + Const.API_ROUTE + DeviceStates.getAndroidId() + Const.API_CONTACTS_ROUTE;
    }

    public void setLock(boolean enabled) {
        editor.putBoolean(Const.IS_DEVICE_LOCKED, enabled);
        editor.commit();
    }

    public void storePhoneNumber(String phoneNumber) {
        editor.putString(Const.OWNER_PHONE_NUMBER, phoneNumber);
        editor.commit();
    }

    public void storeOwnerInfo(String message) {
        editor.putString(Const.OWNER_INFORMATION, message);
        editor.commit();
    }

    public String getOwnerInfo() {
        return pref.getString(Const.OWNER_INFORMATION, null);
    }

    public String getOwnerPhoneNumber() {
        return pref.getString(Const.OWNER_PHONE_NUMBER, null);
    }

    public boolean isLocked() {
        return pref.getBoolean(Const.IS_DEVICE_LOCKED, false);
    }

    public void storeSimcardId(String subscriberId) {
        editor.putString(Const.SIM_SUBSCRIBER_ID, subscriberId);
        editor.commit();
    }

    public String getSimcardId() {
        return pref.getString(Const.SIM_SUBSCRIBER_ID, null);
    }

    public boolean isDeviceAdmin() {
        return pref.getBoolean(Const.IS_DEVICE_ADMIN_ENABLED, false);
    }

    public void setDeviceAdmin(boolean enabled) {
        editor.putBoolean(Const.IS_DEVICE_ADMIN_ENABLED, enabled);
        editor.commit();
    }

    public void setPrefInitSuccess() {
        editor.putBoolean(Const.PREFS_INITIALIZED, true);
        editor.commit();
    }
}
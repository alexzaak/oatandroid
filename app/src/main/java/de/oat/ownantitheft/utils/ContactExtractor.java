package de.oat.ownantitheft.utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import de.oat.ownantitheft.remote.DeviceController;

/**
 * Created by Alexander Zaak on 11.09.15.
 */
public class ContactExtractor {
    private static final Logger LOGGER = Logger.getLogger(ContactExtractor.class);

    private ContactExtractor() {

    }

    public static void extractContacts(Context context) throws IOException {
        if (!PermissionsCheck.hasPermissionReadContacts()) {
            LOGGER.error("permission denied for read contacts");
            return;
        }

        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        phones.moveToFirst();
        // packing the encoded data and IV to Output-stream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        for (int i = 0; i < phones.getCount(); i++) {

            getVCard(context, phones, outputStream);

            phones.moveToNext();

        }
        outputStream.close();
        phones.close();

        DeviceController.sendContacts(outputStream.toByteArray());

    }

    private static void getVCard(Context context, Cursor cursor, ByteArrayOutputStream outputStream) {


        //cursor.moveToFirst();
        String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
        AssetFileDescriptor fd;
        try {
            fd = context.getContentResolver().openAssetFileDescriptor(uri, "r");

            FileInputStream fis = fd.createInputStream();
            byte[] buf = new byte[(int) fd.getDeclaredLength()];
            fis.read(buf);
            String vCardString = new String(buf);


            outputStream.write(vCardString.getBytes());

        } catch (Exception e) {
            LOGGER.error(e);
        }
    }


}

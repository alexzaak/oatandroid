package de.oat.ownantitheft.controller;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import de.oat.ownantitheft.configs.ConfigureLog4J;
import de.oat.ownantitheft.utils.AndroidExceptionHandler;

/**
 * Created by Alexander Zaak on 10.08.15.
 */
public class AppController extends Application {


    private static AppController mInstance;
    private static AndroidExceptionHandler exceptionHandler = new AndroidExceptionHandler();


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static synchronized ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) mInstance.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static synchronized WifiManager getWifiManager() {
        return (WifiManager) mInstance.getSystemService(Context.WIFI_SERVICE);
    }

    public static synchronized TelephonyManager getTelephonyManager() {
        return (TelephonyManager) mInstance.getSystemService(Context.TELEPHONY_SERVICE);
    }


    public static BluetoothAdapter getBluetoothAdapter() {
        return BluetoothAdapter.getDefaultAdapter();
    }

    public static synchronized PackageManager getMainPackageManager() {
        return mInstance.getPackageManager();
    }

    public static String getAppPackageName() {
        return mInstance.getPackageName();
    }

    public static synchronized ApplicationInfo getAppInfo() throws PackageManager.NameNotFoundException {
        PackageInfo paramPackageInfo = mInstance.getPackageManager().getPackageInfo(
                mInstance.getPackageName(), 0);
        return paramPackageInfo.applicationInfo;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        try {
            ConfigureLog4J.configure(this);
        } catch (Exception e) {
            Log.e(AppController.class.getName(), e.getMessage());
        }

        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
    }
}